package shaders
var DefaultVertex = `#version 410

layout (location=0) in vec3 vertex; // vertex position
layout (location=1) in vec4 uv; // per-vertex texture co-ords
layout (location=2) in vec4 color; 
layout (location=3) in vec3 translation;
layout (location=4) in vec3 rotation;
layout (location=5) in vec2 scale;

uniform mat4 projection;
uniform mat4 view;

out vec2 texCoords;
out vec4 vertColor;


void main() {
    texCoords.x = -(vertex.x-0.5)*uv.x  + (vertex.x+0.5)*uv.z;
    texCoords.y = -(vertex.y-0.5)*uv.y  + (vertex.y+0.5)*uv.w;

    mat4 scale_mat = mat4(1.0);
    scale_mat[0][0] = scale.x;
    scale_mat[1][1] = scale.y;

    mat4 rotate_mat_z = mat4(1.0);
    float c = cos(rotation.z);
    float s = sin(rotation.z);
    rotate_mat_z[0][0] = c;
    rotate_mat_z[1][1] = c;
    rotate_mat_z[0][1] = s;
    rotate_mat_z[1][0] = -s;

    mat4 rotate_mat_y = mat4(1.0);
    c = cos(rotation.y);
    s = sin(rotation.y);
    rotate_mat_y[0][0] = c;
    rotate_mat_y[2][2] = c;
    rotate_mat_y[0][2] = -s;
    rotate_mat_y[2][0] = s;

    mat4 rotate_mat_x = mat4(1.0);
    c = cos(rotation.x);
    s = sin(rotation.x);
    rotate_mat_x[1][1] = c;
    rotate_mat_x[2][2] = c;
    rotate_mat_x[1][2] = s;
    rotate_mat_x[2][1] = -s;
    
    mat4 translate_mat = mat4(1.0);
    translate_mat[3] = vec4(translation, 1);

    mat4 model = translate_mat * rotate_mat_x * rotate_mat_y * rotate_mat_z * scale_mat;

    gl_Position =  projection * view  * model * vec4(vertex, 1.0);
    
    vertColor = color;
}




 ` + "\x00" 
