package shaders

// Stores the location index and type of an attribute in the shader
type ShaderAttribute struct {
	Name     string    // Attribute name as it appears in the shader source
	Location uint32    // OpenGL attribute index - assigned with EnableVertexAttribArray
	Type     GLSLType  // GLSL type (vec2, vec3 etc)
	Default  []float32 // default value for this attribute
	Data     []float32 // data of this attribute
}

func (s ShaderAttribute) Copy() ShaderAttribute {
	sa := ShaderAttribute{
		Name:     s.Name,
		Location: s.Location,
		Type:     s.Type,
	}
	sa.Data = make([]float32, s.Type.Size)
	//copy default data
	if len(s.Default) > 0 {
		for i := range sa.Data {
			sa.Data[i] = s.Default[i%len(s.Default)]
		}
	}
	copy(sa.Default, s.Default)

	return sa
}
