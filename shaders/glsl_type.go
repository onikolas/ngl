// GLSL types
package shaders

// GLSLType stores the name and size of a GLSL variable
type GLSLType struct {
	Name string
	Size int32 // number of float32s
}
