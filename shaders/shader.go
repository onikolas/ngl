// OpenGL shader and program compilation utils
package shaders

import (
	"errors"
	"fmt"
	"os"
	"strings"

	"github.com/go-gl/gl/v4.6-core/gl"
)

// Common attributes expected to be in all shaders and in this order. The order should match
// ShaderAttribute.Location. Other (non-common) attributes must come after.
var CommonAttributes = []string{"vertex", "uv", "color", "translation", "rotation", "scale"}

// Shader stores a GLSL vertex and fragment shader pair and provides easy access to shader attributes
type Shader struct {
	Id               int
	Vertex, Fragment string // shader source
	Program          uint32 // opengl program

	// shader parameters - name must match the shader source
	Attributes map[string]ShaderAttribute // named access to shader attributes
	Uniforms   map[string]GLSLType        // named access to shader unifrom parameters
}

// Allocates the storage needed for this shader's attributes
func (s Shader) AttributeData() map[string]ShaderAttribute {
	data := make(map[string]ShaderAttribute)
	for k, v := range s.Attributes {
		data[k] = v
	}
	return data
}

// UpdateUniform changes the value of a uniform variable in the shader. It handles matrices and
// vectors of type GLSLType (float vectors and matrices)
func (s *Shader) UpdateUniform(name string, value []float32) error {
	if _, ok := s.Uniforms[name]; !ok {
		return errors.New("Attribute not found")
	}

	loc := gl.GetUniformLocation(s.Program, gl.Str(name+"\x00"))

	switch s.Uniforms[name].Name {
	case "float":
		gl.Uniform1f(loc, value[0])
	case "vec2":
		gl.Uniform2f(loc, value[0], value[1])
	case "vec3":
		gl.Uniform3f(loc, value[0], value[1], value[2])
	case "vec4":
		gl.Uniform4f(loc, value[0], value[1], value[2], value[3])
	case "mat4":
		gl.UniformMatrix4fv(loc, 1, false, &(value[0]))
	default:
		return errors.New("Unknown uniform name")
	}

	return nil
}

// NewShader creates a Shader with given sources and attributes. A simple sanity check is performed
// which checks that the attribute name and type are present in the shader source.
// TODO: check that order of common sprite attributes matches CommonAttributes
func NewShader(vertexShader, fragmentShader string, attributes map[string]ShaderAttribute,
	uniforms map[string]GLSLType, id int) (Shader, error) {

	shader := Shader{}
	var err error
	if shader.Program, err = createGLProgram(vertexShader, fragmentShader); err != nil {
		return shader, err
	}

	shader.Attributes = attributes
	shader.Uniforms = uniforms
	shader.Vertex = vertexShader
	shader.Fragment = fragmentShader
	shader.Id = id

	shaderLines := strings.SplitAfter(vertexShader, "\n")
	for name, attr := range attributes {
		found := false
		for _, line := range shaderLines {
			if strings.Contains(line, name) && strings.Contains(line, attr.Type.Name) {
				found = true
				break
			}
		}
		if !found {
			return shader, errors.New(fmt.Sprint("Attribute not found: ", name, attr.Type))
		}
	}

	// uniforms can be in both shaders
	shaderLines = append(shaderLines, strings.SplitAfter(fragmentShader, "\n")...)
	for name, gltype := range uniforms {
		found := false
		for _, line := range shaderLines {
			if strings.Contains(line, name) && strings.Contains(line, gltype.Name) {
				found = true
				break
			}
		}
		if !found {
			return shader, errors.New(fmt.Sprintf("Uniform not found: %v %v", name, gltype.Name))
		}
	}

	return shader, nil
}

// Same as NewShader but shaders are loaded from files
func NewShaderFromFiles(vertexShaderFilename, fragmentShaderFilename string,
	attributes map[string]ShaderAttribute, uniforms map[string]GLSLType, id int) (Shader, error) {

	vertSource, err := shaderSourceFromFile(vertexShaderFilename)
	if err != nil {
		return Shader{}, err
	}
	fragSource, err := shaderSourceFromFile(fragmentShaderFilename)
	if err != nil {
		return Shader{}, err
	}

	return NewShader(vertSource, fragSource, attributes, uniforms, id)
}

// Create an opengl program
func createGLProgram(vertexShader, fragmentShader string) (uint32, error) {
	var err error
	var VertexShader, FragmentShader uint32
	if VertexShader, err = compileShader(vertexShader, gl.VERTEX_SHADER); err != nil {
		return 0, err
	}
	if FragmentShader, err = compileShader(fragmentShader, gl.FRAGMENT_SHADER); err != nil {
		return 0, err
	}

	prog := gl.CreateProgram()
	gl.AttachShader(prog, VertexShader)
	gl.AttachShader(prog, FragmentShader)
	gl.LinkProgram(prog)

	return prog, nil
}

// compileShader given in source. shaderType can be gl.VERTEX_SHADER / gl.FRAGMENT_SHADER
func compileShader(source string, shaderType uint32) (uint32, error) {
	shader := gl.CreateShader(shaderType)

	csources, free := gl.Strs(source)
	gl.ShaderSource(shader, 1, csources, nil)
	free()
	gl.CompileShader(shader)

	var status int32
	gl.GetShaderiv(shader, gl.COMPILE_STATUS, &status)
	if status == gl.FALSE {
		var logLength int32
		gl.GetShaderiv(shader, gl.INFO_LOG_LENGTH, &logLength)

		log := strings.Repeat("\x00", int(logLength+1))
		gl.GetShaderInfoLog(shader, logLength, nil, gl.Str(log))

		return 0, fmt.Errorf("failed to compile %v: %v", source, log)
	}

	return shader, nil
}

// helper to load shaders from file
func shaderSourceFromFile(filename string) (string, error) {
	bytes, err := os.ReadFile(filename)

	if err != nil {
		return "", err
	}
	return string(bytes) + "\x00", nil
}
