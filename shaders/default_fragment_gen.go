package shaders
var DefaultFragment = `#version 410

uniform sampler2D tex;

in vec2 texCoords;
in vec4 vertColor;
out vec4 frag_colour;

void main() {
    vec4 texel = texture(tex, texCoords);
    if(texel.a < 0.5) discard;
    frag_colour = texel * vertColor ;
} 
 ` + "\x00" 
