package shaders

//go:generate shader-gen -source source/color_sprite_instanced.vertex -package shaders -var DefaultVertex -out default_vertex_gen.go
//go:generate shader-gen -source source/color_sprite.fragment -package shaders -var DefaultFragment -out default_fragment_gen.go
func NewDefaultShader() (Shader, error) {
	var id = SHIDColorSpriteShader
	attributes := map[string]ShaderAttribute{
		"vertex": {
			Name:     "vertex",
			Location: 0,
			Type:     GLSLType{"vec3", 3},
		},
		"uv": {
			Name:     "uv",
			Location: 1,
			Type:     GLSLType{"vec4", 4},
		},
		"color": {
			Name:     "color",
			Location: 2,
			Type:     GLSLType{"vec4", 4},
			Default:  []float32{1, 1, 1, 1},
		},
		"translation": {
			Name:     "translation",
			Location: 3,
			Type:     GLSLType{"vec3", 3},
			Default:  []float32{0, 0, 0},
		},
		"rotation": {
			Name:     "rotation",
			Location: 4,
			Type:     GLSLType{"vec3", 3},
			Default:  []float32{0, 0, 0},
		},
		"scale": {
			Name:     "scale",
			Location: 5,
			Type:     GLSLType{"vec2", 2},
			Default:  []float32{1, 1},
		},
	}

	uniforms := map[string]GLSLType{
		"projection": {"mat4", 16},
		"view":       {"mat4", 16},
	}

	shader, err := NewShader(DefaultVertex, DefaultFragment, attributes, uniforms, id)
	if err != nil {
		panic(err)
	}
	return shader, nil
}
