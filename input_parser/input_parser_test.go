package input

import (
	"testing"
	"time"
)

const LeafNodeAction InputAction = 10

func TestInputParserAddSequence(t *testing.T) {
	ip := InputParser{}
	sequence := []*ParseTreeNode{
		{
			Input: &KeyboardEvent{
				Key: KeyLeft,
			},
		},
		{
			Input: &KeyboardEvent{
				Key: KeyLeft,
			},
			Action: LeafNodeAction,
		},
	}

	ip.AddSequence(sequence)

	if !ip.parserRoot.children[0].Input.Match(&KeyboardEvent{Key: KeyLeft}) {
		t.Error(ip.parserRoot.children[0].Input)
	}
	if !ip.parserRoot.children[0].children[0].Input.Match(&KeyboardEvent{Key: KeyLeft}) {
		t.Error(ip.parserRoot.children[0].children[0].Input)
	}

	sequence = []*ParseTreeNode{
		{
			Input: &KeyboardEvent{
				Key: KeyLeft,
			},
		},
		{
			Input: &KeyboardEvent{
				Key: KeyRight,
			},
			Action: LeafNodeAction,
		},
	}

	ip.AddSequence(sequence)

	if !ip.parserRoot.children[0].Input.Match(&KeyboardEvent{Key: KeyLeft}) {
		t.Error(ip.parserRoot.children[0].Input)
	}
	if !ip.parserRoot.children[0].children[0].Input.Match(&KeyboardEvent{Key: KeyLeft}) {
		t.Error(ip.parserRoot.children[0].children[0].Input)
	}
	if !ip.parserRoot.children[0].children[1].Input.Match(&KeyboardEvent{Key: KeyRight}) {
		t.Error(ip.parserRoot.children[0].children[0].Input)
	}

	sequence = []*ParseTreeNode{
		{
			Input: &MouseEvent{
				MouseAction: MouseLeftDown,
			},
		},
		{
			Input: &KeyboardEvent{
				Key: KeyRight,
			},
			Action: LeafNodeAction,
		},
	}

	ip.AddSequence(sequence)

	if !ip.parserRoot.children[0].Input.Match(&KeyboardEvent{Key: KeyLeft}) {
		t.Error(ip.parserRoot.children[0].Input)
	}
	if !ip.parserRoot.children[0].children[0].Input.Match(&KeyboardEvent{Key: KeyLeft}) {
		t.Error(ip.parserRoot.children[0].children[0].Input)
	}
	if !ip.parserRoot.children[0].children[1].Input.Match(&KeyboardEvent{Key: KeyRight}) {
		t.Error(ip.parserRoot.children[0].children[0].Input)
	}
	if !ip.parserRoot.children[1].Input.Match(&MouseEvent{MouseAction: MouseLeftDown}) {
		t.Error(ip.parserRoot.children[0].Input)
	}
	if !ip.parserRoot.children[1].children[0].Input.Match(&KeyboardEvent{Key: KeyRight}) {
		t.Error(ip.parserRoot.children[0].children[0].Input)
	}

	sequence = []*ParseTreeNode{
		{
			Input: &MouseEvent{
				MouseAction: MouseLeftDown,
			},
		},
	}

	if err := ip.AddSequence(sequence); err == nil {
		t.Error("Expecting error due to missing action on leaf node")
	}

}

func TestInputParserParse(t *testing.T) {
	ip := InputParser{}

	if match, fn, _ := ip.Parse(); match != MatchTypeEmpty || fn != InputActionNone {
		t.Error(match, fn)
	}

	ip.BufferInput(&KeyboardEvent{Key: KeyDelete})
	if match, fn, _ := ip.Parse(); match != MatchTypeMiss || fn != 0 {
		t.Error(match, fn)
	}

	sequence := []*ParseTreeNode{
		{
			Input: &KeyboardEvent{
				Key: KeyLeft,
			},
		},
		{
			Input: &KeyboardEvent{
				Key: KeyLeft,
			},
			Action: LeafNodeAction,
		},
	}

	ip.AddSequence(sequence)

	ip.BufferInput(&KeyboardEvent{Key: KeyDelete})
	if match, fn, _ := ip.Parse(); match != MatchTypeMiss || fn != InputActionNone {
		t.Error(match, fn)
	}

	ip.BufferInput(&KeyboardEvent{Key: KeyLeft})
	if match, fn, _ := ip.Parse(); match != MatchTypePartial || fn != InputActionNone {
		t.Error(match, fn)
	}
	ip.BufferInput(&KeyboardEvent{Key: KeyRight})
	if match, fn, _ := ip.Parse(); match != MatchTypeMiss || fn != InputActionNone {
		t.Error(match, fn)
	}

	if len(ip.inputBuffer) != 0 {
		t.Error("input buffer not cleared")
	}

	ip.BufferInput(&KeyboardEvent{Key: KeyLeft})
	if match, fn, _ := ip.Parse(); match != MatchTypePartial || InputActionNone != InputActionNone {
		t.Error(match, fn)
	}
	ip.BufferInput(&KeyboardEvent{Key: KeyLeft})
	match, fn, inputs := ip.Parse()
	if match != MatchTypeFull || fn == 0 {
		t.Error(match, fn)
	}
	ke, ok := inputs[0].(*KeyboardEvent)
	if !ok {
		t.Error()
	}
	if ke.Key != KeyLeft {
		t.Error()
	}

	// Tests DiscardMissInputs mechanism
	// ---------------------------------

	sequence = []*ParseTreeNode{
		{
			Input: &KeyboardEvent{
				Key:  KeyText,
				Text: "a",
			},
			DiscardMissInputs: 2,
		},
		{
			Input: &KeyboardEvent{
				Key:  KeyText,
				Text: "b",
			},
			Action: LeafNodeAction,
		},
	}
	if err := ip.AddSequence(sequence); err != nil {
		t.Error(err)
	}

	// Correct input, this node has 2 discards
	ip.BufferInput(&KeyboardEvent{Key: KeyText, Text: "a"})
	if match, fn, _ := ip.Parse(); match != MatchTypePartial || fn != InputActionNone {
		t.Error(match, fn)
	}
	// these two are missmatches but get descarded
	ip.BufferInput(&KeyboardEvent{Key: KeyText, Text: "a"})
	if match, fn, _ := ip.Parse(); match != MatchTypePartial || fn != InputActionNone {
		t.Error(match, fn)
	}
	ip.BufferInput(&KeyboardEvent{Key: KeyText, Text: "a"})
	if match, fn, _ := ip.Parse(); match != MatchTypePartial || fn != InputActionNone {
		t.Error(match, fn)
	}
	// also missmatch but no discards left so miss
	ip.BufferInput(&KeyboardEvent{Key: KeyText, Text: "a"})
	if match, fn, _ := ip.Parse(); match != MatchTypeMiss || fn != InputActionNone {
		t.Error(match, fn)
	}
	// Same sequence except the last one matches
	ip.BufferInput(&KeyboardEvent{Key: KeyText, Text: "a"})
	if match, fn, _ := ip.Parse(); match != MatchTypePartial || fn != InputActionNone {
		t.Error(match, fn)
	}
	ip.BufferInput(&KeyboardEvent{Key: KeyText, Text: "a"})
	if match, fn, _ := ip.Parse(); match != MatchTypePartial || fn != InputActionNone {
		t.Error(match, fn)
	}
	ip.BufferInput(&KeyboardEvent{Key: KeyText, Text: "c"})
	if match, fn, _ := ip.Parse(); match != MatchTypePartial || fn != InputActionNone {
		t.Error(match, fn)
	}
	ip.BufferInput(&KeyboardEvent{Key: KeyText, Text: "b"})
	if match, fn, _ := ip.Parse(); match != MatchTypeFull || fn == InputActionNone {
		t.Error(match, fn)
	}

	// Tests timing
	// ------------

	sequence = []*ParseTreeNode{
		{
			Input: &KeyboardEvent{
				Key:  KeyText,
				Text: "timing",
			},
		},
		{
			Input: &KeyboardEvent{
				Key:  KeyText,
				Text: "next",
			},
			Action: LeafNodeAction,
			DtMin:  time.Second * 2,
			DtMax:  time.Second * 4,
		},
	}
	if err := ip.AddSequence(sequence); err != nil {
		t.Error(err)
	}

	now := time.Now()
	ip.BufferInput(&KeyboardEvent{Key: KeyText, Text: "timing", Timestamp: now})
	if match, fn, _ := ip.Parse(); match != MatchTypePartial || fn != InputActionNone {
		t.Error(match, fn)
	}
	// too early
	ip.BufferInput(&KeyboardEvent{Key: KeyText, Text: "next", Timestamp: now.Add(time.Second)})
	if match, fn, _ := ip.Parse(); match != MatchTypeMiss || fn != InputActionNone {
		t.Error(match, fn)
	}
	ip.BufferInput(&KeyboardEvent{Key: KeyText, Text: "timing", Timestamp: now})
	if match, fn, _ := ip.Parse(); match != MatchTypePartial || fn != InputActionNone {
		t.Error(match, fn)
	}
	// too late
	ip.BufferInput(&KeyboardEvent{Key: KeyText, Text: "next", Timestamp: now.Add(time.Second * 5)})
	if match, fn, _ := ip.Parse(); match != MatchTypeMiss || fn != InputActionNone {
		t.Error(match, fn)
	}
	ip.BufferInput(&KeyboardEvent{Key: KeyText, Text: "timing", Timestamp: now})
	if match, fn, _ := ip.Parse(); match != MatchTypePartial || fn != InputActionNone {
		t.Error(match, fn)
	}
	// on time
	ip.BufferInput(&KeyboardEvent{Key: KeyText, Text: "next", Timestamp: now.Add(time.Second * 3)})
	if match, fn, _ := ip.Parse(); match != MatchTypeFull || fn == InputActionNone {
		t.Error(match, fn)
	}

	// Test multiple inputs with the same pattern but different timing	TODO
	sequence = []*ParseTreeNode{
		{
			Input: &KeyboardEvent{
				Key:  KeyText,
				Text: "timing",
			},
		},
		{
			Input: &KeyboardEvent{
				Key:  KeyText,
				Text: "next",
			},
			Action: LeafNodeAction,
			DtMin:  time.Second * 1,
			DtMax:  time.Second * 10,
		},
	}
	if err := ip.AddSequence(sequence); err != nil {
		t.Error(err)
	}

	// Test timeout clearing the input buffer
	// --------------------------------------
	ip.SetTimeout(time.Second * 2)
	ip.BufferInput(&KeyboardEvent{Key: KeyText, Text: "timing", Timestamp: now})
	if match, fn, _ := ip.Parse(); match != MatchTypePartial || fn != InputActionNone {
		t.Error(match, fn)
	}
	// would be a miss but the buffer is cleared before adding so it becames a partial
	ip.BufferInput(&KeyboardEvent{Key: KeyText, Text: "timing", Timestamp: now.Add(time.Second * 3)})
	if match, fn, _ := ip.Parse(); match != MatchTypePartial || fn != InputActionNone {
		t.Error(match, fn)
		t.Log(ip.inputBuffer)
	}

	ip.ClearBuffer()
	ip.EnableTextParsing()
	ip.BufferInput(&KeyboardEvent{Key: KeyText, Text: "sometext", Timestamp: now})
	match, fn, text := ip.Parse()
	if match != MatchTypeFull || fn != InputActionText {
		t.Error(match, fn)
	}
	txt, ok := text[0].(*KeyboardEvent)
	if !ok {
		t.Error("casting")
	}
	if txt.Text != "sometext" {
		t.Error(txt)
	}
}
