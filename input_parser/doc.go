// Provides parsing of input events into actions. Currently supports keyboard and
// mouse (gamepad on the way).
package input
