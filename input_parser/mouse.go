package input

import (
	"time"

	"gitlab.com/onikolas/math"
)

type MouseAction int

//go:generate stringer -type=MouseAction
const (
	MouseLeftDown MouseAction = iota
	MouseLeftUp
	MouseRightDown
	MouseRightUp
	MouseMove
)

type MouseEvent struct {
	MouseAction MouseAction       //button pressed or move
	Delta       math.Vector2[int] // mouse movement
	Timestamp   time.Time
}

func (m *MouseEvent) Match(other InputEventMatcher) bool {
	mother, ok := other.(*MouseEvent)
	if !ok {
		return false
	}

	return mother.MouseAction == m.MouseAction
}

func (m *MouseEvent) GetTimestamp() time.Time {
	return m.Timestamp
}
