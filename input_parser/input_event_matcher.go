package input

import "time"

// InputEventMatcher defines a Match function that lets us compare input events
type InputEventMatcher interface {
	Match(InputEventMatcher) bool
	GetTimestamp() time.Time
}
