package input

import "testing"

func TestInputEventMatcher(t *testing.T) {
	toMatch := []InputEventMatcher{
		&KeyboardEvent{Key: KeyRight},
		&KeyboardEvent{Key: KeyText, Text: "a"},
		&MouseEvent{MouseAction: MouseLeftUp},
	}

	inputs := [][]InputEventMatcher{
		{
			&KeyboardEvent{Key: KeyRight},
			&KeyboardEvent{Key: KeyText, Text: "b"},
			&MouseEvent{MouseAction: MouseLeftUp},
		},
		{
			&KeyboardEvent{Key: KeyText, Text: "a"},
			&MouseEvent{MouseAction: MouseLeftUp},
			&KeyboardEvent{Key: KeyRight},
		},
		{
			&KeyboardEvent{Key: KeyRight},
			&KeyboardEvent{Key: KeyText, Text: "a"},
			&MouseEvent{MouseAction: MouseLeftUp},
		},
	}

	isMatch := []bool{false, false, true}

	for i := range inputs {
		match := true
		for j := range inputs[i] {
			if !inputs[i][j].Match(toMatch[j]) {
				match = false
			}
		}
		if match != isMatch[i] {
			t.Error(i, inputs[i])
		}
	}
}
