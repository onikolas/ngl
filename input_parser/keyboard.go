package input

import (
	"fmt"
	"time"
)

type KeyboardKey int

//go:generate stringer -type=KeyboardKey
const (
	// Keyboard Events
	// These only control
	KeyNone KeyboardKey = iota
	KeyControlDown
	KeyControlUp
	KeyAltDown
	KeyAltUp
	KeyBackspace
	KeyDelete
	KeyLeft
	KeyRight
	KeyUp
	KeyDown
	// These have text
	KeySpace
	KeyNewline
	KeyText
)

func (k KeyboardKey) HasText() bool {
	if k <= KeyDown {
		return false
	}
	return true
}

// KeyboardEvent encodes keyboards inputs
type KeyboardEvent struct {
	Key       KeyboardKey // key that was pressed
	Text      string      // text (if any)
	Timestamp time.Time   // time.Now() when the key was pressed
}

// Create a new KeyBoardEvent for key k
func NewKeyBoardEvent(k KeyboardKey) *KeyboardEvent {
	return &KeyboardEvent{Key: k}
}

// Create a new KeyBoardEvent that has text. Newline and space are also text but have their own
// KeyboardKey enumeration.
func NewKeyBoardEventText(text string) *KeyboardEvent {
	if text == "\n" {
		return &KeyboardEvent{Key: KeyNewline, Text: text}
	}
	if text == " " {
		return &KeyboardEvent{Key: KeySpace, Text: text}
	}
	return &KeyboardEvent{Key: KeyText, Text: text}
}

// Create a KeyboardEvent from an abbreviation.
func NewKeyBoardEventAbbr() {

}

func (k *KeyboardEvent) Match(other InputEventMatcher) bool {
	kother, ok := other.(*KeyboardEvent)
	if !ok {
		return false
	}

	return kother.Key == k.Key && kother.Text == k.Text
}

func (k *KeyboardEvent) GetTimestamp() time.Time {
	return k.Timestamp
}

func (k *KeyboardEvent) String() string {
	return fmt.Sprintf("%v %v", k.Key, k.Text)
}
