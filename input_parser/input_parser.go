package input

import (
	"errors"
	"fmt"
	"time"
)

// InputAction is used for enumerating successful parses of input sequences.
type InputAction int

const (
	InputActionNone                 = 0 // means a parse resulted in a partial or a miss
	InputActionReserved InputAction = 100000000
	InputActionText                 = InputActionReserved + 2 // text input - used if EnableTextParsing() was called
)

// ParseTreeNode is used to construct a parse tree that translates input sequences into actions
type ParseTreeNode struct {
	// the input to match at this node
	Input InputEventMatcher

	// Min/Max time between this input and its parent. If the input arrives outside this window, its a miss
	// Ignored if either is zero
	DtMin, DtMax time.Duration

	// Number of false inputs after this one to discard. If it reaches zero a missmatch causes the whole input buffer
	// to be emptied. Otherwise only the erroneous input is discarded
	DiscardMissInputs int

	// Action to perform on full match. Ignored on non-leaf nodes
	Action InputAction

	children []*ParseTreeNode
}

// InputParser buffers InputEvents and parses them. On a successfull match it returns a function
// which includes the triggering input sequence so it can be acted upon
type InputParser struct {
	inputBuffer   []InputEventMatcher
	lastInputTime time.Time
	timeout       time.Duration
	parserRoot    *ParseTreeNode
	parseText     bool

	// keeps track of discards in subsequent Parse() calls
	discarded int
}

// BufferInput buffers an input. If the time difference between the this and the last input is
// greater than timeout, the buffer is cleared before adding. Typically, we would call Parse
// afterwards to check if the input resulted in a match.
func (ip *InputParser) BufferInput(input InputEventMatcher) {
	inputTime := input.GetTimestamp()
	if ip.timeout > 0 && !ip.lastInputTime.IsZero() && inputTime.Sub(ip.lastInputTime) > ip.timeout {
		ip.ClearBuffer()
	}
	ip.inputBuffer = append(ip.inputBuffer, input)
	ip.lastInputTime = inputTime
}

// Set the timeout after which the input buffer gets cleared due to inactivity
func (ip *InputParser) SetTimeout(t time.Duration) {
	ip.timeout = t
}

// EnableTextParsing sets the InputParser to accept text without having to specify rules in the
// parse tree. Text inputs will result in a InputActionText result when Parse is called and the text
// KeyboardEvent will be returned with the text string embedded.
func (ip *InputParser) EnableTextParsing() {
	ip.parseText = true
}

type MatchType int

//go:generate stringer -type=MatchType
const (
	MatchTypeFull MatchType = iota
	MatchTypePartial
	MatchTypeMiss
	MatchTypeEmpty
)

// Parse runs the contents of the input buffer through the parse tree and looks for matches. It can result in:
// 1.Full match: Whole buffer matched and got an action. Buffer is cleared.
// 2.Partial match: the whole of input buffer matches but the last node did not have an action. Means that subsequent
// inputs are needed. Buffer is not cleared.
// 3. Missmatch: The input buffer does not match anything in the parse tree. Buffer is cleared.
// 4. Empty: Parse was called but the input buffer is empty
func (ip *InputParser) Parse() (MatchType, InputAction, []InputEventMatcher) {
	if len(ip.inputBuffer) == 0 {
		return MatchTypeEmpty, InputActionNone, nil
	}

	// special handling for text - this avoids having to add tree nodes for each text key
	if ip.parseText {
		keyboardEvent, ok := ip.inputBuffer[0].(*KeyboardEvent)
		if ok && keyboardEvent.Key.HasText() {
			ip.ClearBuffer()
			return MatchTypeFull, InputActionText, []InputEventMatcher{keyboardEvent}
		}
	}

	node := ip.parserRoot
	matches := []InputEventMatcher{}
	var prevTimestamp time.Time

	for i := range ip.inputBuffer {
		matched := false

		if node == nil {
			break
		}

		for j := range node.children {
			if node.children[j].DtMin > 0 && node.children[j].DtMax > 0 {
				dt := ip.inputBuffer[i].GetTimestamp().Sub(prevTimestamp)
				if dt < node.children[j].DtMin || dt > node.children[j].DtMax {
					continue // treat it as missmatch even if Input.Match wouldn't
				}
			}

			if node.children[j].Input.Match(ip.inputBuffer[i]) {
				node = node.children[j]
				matched = true
				prevTimestamp = ip.inputBuffer[i].GetTimestamp()
				break //stop on the 1st match
			}
		}

		if !matched {
			// track removed inputs until they equal node.DiscardMissInputs
			if ip.discarded < node.DiscardMissInputs {
				ip.inputBuffer = append(ip.inputBuffer[0:i], ip.inputBuffer[i+1:]...)
				ip.discarded++
			}
			break
		}

		matches = append(matches, ip.inputBuffer[i])
	}

	if len(matches) < len(ip.inputBuffer) {
		ip.ClearBuffer()
		ip.discarded = 0
		return MatchTypeMiss, InputActionNone, nil
	}

	if node.Action == InputActionNone {
		return MatchTypePartial, InputActionNone, nil
	}

	ip.ClearBuffer()
	return MatchTypeFull, node.Action, matches
}

// AddSequence adds the sequence to the parse tree. The last node must contain an action
func (ip *InputParser) AddSequence(sequence []*ParseTreeNode) error {
	if sequence[len(sequence)-1].Action == InputActionNone {
		return errors.New("Leaf node must have action")
	}

	if ip.parserRoot == nil {
		ip.parserRoot = &ParseTreeNode{}
	}

	node := ip.parserRoot
	matchIndex := 0

	for i := range sequence {
		matched := false
		for j := range node.children {
			if node.children[j].Input.Match(sequence[i].Input) {
				node = node.children[j]
				matched = true
				break
			}
		}
		if !matched {
			matchIndex = i
			break
		}
	}

	// add the remaining sequence
	for i := matchIndex; i < len(sequence); i++ {
		node.children = append(node.children, sequence[i])
		node = sequence[i]
	}

	return nil
}

// ClearBuffer empties the input buffer
func (ip *InputParser) ClearBuffer() {
	ip.inputBuffer = ip.inputBuffer[0:0]
}

func PrintTree(node *ParseTreeNode, depth int) {
	for i := 0; i < depth; i++ {
		fmt.Print("-")
	}
	fmt.Println(node.Input)
	for i := range node.children {
		PrintTree(node.children[i], depth+1)
	}
}
