package ui

func Zero[T any]() T {
	return *new(T)
}

// FindByType searches a UI hierarchy for an element of type T
func FindByType[T Element](start Element) (T, bool) {
	if _, ok := start.(T); ok {
		return start.(T), true
	}
	for _, c := range start.GetChildren() {
		if t, ok := FindByType[T](c); ok {
			return t, ok
		}
	}
	return Zero[T](), false
}

// Find an element which matches name
func FindByName(start Element, name string) (Element, bool) {
	if start.GetName() == name {
		return start, true
	}
	for _, c := range start.GetChildren() {
		if t, ok := FindByName(c, name); ok {
			return t, ok
		}
	}
	return nil, false
}
