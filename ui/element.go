package ui

import (
	"time"

	"gitlab.com/onikolas/math"
	"gitlab.com/onikolas/agl/sprite"
)

// Element is the base interface for UI elements.
type Element interface {
	// Update determines the ui's position and size which can be relevant to its parent.
	// Depth is the tree depth wich is typically used to set the front to back order of elements
	// (deeper=front).
	// dt is the time since the last update wich is used in element updates that are
	// time sensitive such as animations. Update's implementation is expected to call Update on the
	// element's children:
	// 	for _, c := range a.Children {
	//	    c.Update(depth, dt)
	//  }
	Update(depth int, dt time.Duration)

	// AddChild adds an Element to the UI.Children array. The implementaion is expected to also set
	// itself as the child's parent:
	//	a.Children = append(a.Children, child)
	//  child.SetParent(a)
	AddChild(child Element)

	// SetParent sets this Element's parent
	SetParent(parent Element)

	// BoundingBoxt retuns this Element's BoundingBox
	BoundingBox() math.Box2D[int]

	// GetChildren returns the Element's children nodes
	GetChildren() []Element

	// GetName retursns the Element's name
	GetName() string

	// All UI elements are drawable using sprites but may choose to ruturn no sprites
	sprite.SpriteDrawer
}

// Common data for UI elements
type UI struct {
	Name        string
	Children    []Element
	Parent      Element
	BoundingBox math.Box2D[int]
}
