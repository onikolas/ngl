package ui

import (
	"fmt"
	"time"

	"gitlab.com/onikolas/math"
	"gitlab.com/onikolas/agl/sprite"
)

type Animation struct {
	AnimationSpeed float64 // sprites per second
	frameTime      time.Duration
	waitTime       time.Duration

	indexRange   math.Vector2[int]
	sprites      []sprite.Sprite
	activeSprite int
	UI
}

// Create a new animation. indexRange is the indices [start, end) of the sprites in the atlas.
// animationSpeed controls how many sprites are shown  per second
func NewAnimation(indexRange math.Vector2[int], animationSpeed float64) *Animation {
	sp := &Animation{
		indexRange:     indexRange,
		AnimationSpeed: animationSpeed,
	}
	sp.SetAnimationSpeed(animationSpeed)

	sp.sprites = make([]sprite.Sprite, indexRange.Y-indexRange.X+1)
	var err error
	for i := range sp.sprites {
		sp.sprites[i], err = atlasManager.atlases[ImageAtlas].NewSprite(i + indexRange.X)
		if err != nil {
			panic(err)
		}
	}

	return sp
}

// Create an animation from a list of images
func NewAnimationFromImages(images []string, animationSpeed float64) (*Animation, error) {
	indices := []int{}
	for i := range images {
		img, err := sprite.RgbaFromFile(images[i])
		if err != nil {
			return nil, err
		}
		index, err := atlasManager.atlases[ImageAtlas].AddImage(img)
		if err != nil {
			return nil, err
		}
		indices = append(indices, index)
	}

	return NewAnimation(math.Vector2[int]{indices[0], indices[len(indices)-1]}, animationSpeed), nil
}

func NewAnimationFromAtlasFiles(atlasImage, boundingBoxFile string, animationSpeed float64) (*Animation, error) {
	_, indices, err := atlasManager.atlases[ImageAtlas].AddAtlasImageFromFiles(atlasImage, boundingBoxFile)
	fmt.Println(indices)
	if err != nil {
		return nil, err
	}
	return NewAnimation(math.Vector2[int]{indices[0], indices[len(indices)-1]}, animationSpeed), nil
}

func (a *Animation) Update(depth int, dt time.Duration) {
	a.waitTime += dt
	if a.waitTime >= a.frameTime {
		a.activeSprite = (a.activeSprite + 1) % len(a.sprites)
		a.waitTime = a.waitTime - a.frameTime
	}
	a.UI.BoundingBox = a.Parent.BoundingBox()
	a.sprites[a.activeSprite].SetPosition(a.UI.BoundingBox.P1)
	a.sprites[a.activeSprite].SetDepth(depth - 1)
	a.sprites[a.activeSprite].SetSize(a.UI.BoundingBox.Size())

	for _, c := range a.Children {
		c.Update(depth-1, dt)
	}
}

func (a *Animation) AddChild(child Element) {
	a.Children = append(a.Children, child)
	child.SetParent(a)
}

func (a *Animation) SetParent(parent Element) {
	a.Parent = parent
}

func (a *Animation) BoundingBox() math.Box2D[int] {
	return a.UI.BoundingBox
}

func (a *Animation) GetChildren() []Element {
	return a.Children
}

func (a *Animation) Draw() {
	a.sprites[a.activeSprite].QueueRender()
}

func (a *Animation) String() string {
	return "Sprite"
}

func (a *Animation) GetName() string {
	return a.Name
}

// Set the animation speed in sprites/second
func (a *Animation) SetAnimationSpeed(sps float64) {
	a.AnimationSpeed = sps
	a.frameTime = time.Duration((float64(time.Second) / sps))
	fmt.Println("Frametime: ", a.frameTime)
}
