package ui

import (
	"fmt"
	"image"

	"gitlab.com/onikolas/agl/shaders"
	"gitlab.com/onikolas/agl/sprite"
	"gitlab.com/onikolas/agl/text"
	"gitlab.com/onikolas/math"
)

var atlasManager Manager

const (
	atlasDefaultSize = 4096

	ImageAtlas = 0
)

// Manages common resources needed to render a ui
type Manager struct {
	atlases     map[int]*sprite.Atlas
	textDrawers map[string]*text.TextDrawer
	renderer    *sprite.Renderer
}

func (m *Manager) init() {
	var err error
	atlasManager.atlases = map[int]*sprite.Atlas{}
	atlasManager.textDrawers = map[string]*text.TextDrawer{}
	blank := image.NewRGBA(image.Rect(0, 0, atlasDefaultSize, atlasDefaultSize))
	shader, err := shaders.NewColorSpriteShader()
	if err != nil {
		panic(err)
	}
	atlasManager.atlases[ImageAtlas], err = sprite.NewSpriteAtlas(blank, nil, shader, 4096, 1)
	if err != nil {
		panic(err)
	}
}

// Add an image to the internal image storage
func (m *Manager) AddImage(filename string) (int, error) {
	img, err := sprite.RgbaFromFile(filename)
	if err != nil {
		return -1, err
	}
	index, err := m.atlases[ImageAtlas].AddImage(img)
	if err != nil {
		return -1, err
	}
	return index, nil
}

func fontId(fontName string, fontSize int) string {
	return fmt.Sprintf("%v-%v", fontName, fontSize)
}

func (m *Manager) AddFontFile(filename string, fontSize int, characterSet []rune) error {
	id := fontId(filename, fontSize)
	if _, ok := m.textDrawers[id]; ok {
		return nil
	}
	font := text.FontAtlas{}
	err := font.InitFromFile(filename, float64(fontSize), characterSet)
	if err != nil {
		return err
	}
	shader, err := text.NewTextShader()
	if err != nil {
		return err
	}
	m.textDrawers[id], err = text.NewTextDrawer(&font, math.Box2D[int]{}, 0, shader, atlasDefaultSize, 1)
	m.renderer.AddSpriteStore(m.textDrawers[id].SpriteStore)
	return nil
}

func (m *Manager) AddFont(fontBytes []byte, fontName string, fontSize int, characterSet []rune) error {
	id := fontId(fontName, fontSize)
	if _, ok := m.textDrawers[id]; ok {
		return nil
	}
	font := text.FontAtlas{}
	err := font.Init(fontBytes, float64(fontSize), characterSet)
	if err != nil {
		return err
	}
	shader, err := text.NewTextShader()
	if err != nil {
		return err
	}
	m.textDrawers[id], err = text.NewTextDrawer(&font, math.Box2D[int]{}, 0, shader, atlasDefaultSize, 1)
	m.renderer.AddSpriteStore(m.textDrawers[id].SpriteStore)
	return nil
}

func registerRenderer(renderer *sprite.Renderer) {
	atlasManager.init()
	atlasManager.renderer = renderer
	for _, v := range atlasManager.atlases {
		renderer.AddSpriteStore(v)
	}
}

// Starting poing for a UI. Pass a sprite.Renderer to be used to render this UI. Returns the main
// window of the UI. See MainWindow for how to use.
func New(renderer *sprite.Renderer) *MainWindow {
	registerRenderer(renderer)
	return &MainWindow{}
}

// Register a font to be used in TextStrings and the like. Returns the font's id which is
// "fontFile-fontSize"
func RegisterFont(fontFile string, fontSize int, characterSet []rune) string {
	atlasManager.AddFontFile(fontFile, fontSize, characterSet)
	return fontId(fontFile, fontSize)
}

// Same as RegisterFont but the font data is in []byte format (e.g coming from golang.org/x/image/font/gofont).
func RegisterFontBytes(fontBytes []byte, fontName string, fontSize int, characterSet []rune) string {
	atlasManager.AddFont(fontBytes, fontName, fontSize, characterSet)
	return fontId(fontName, fontSize)
}

// dump a bunch of debug info
func Debug() {
	atlasManager.atlases[ImageAtlas].DumpAtlas()
}
