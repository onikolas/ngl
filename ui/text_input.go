package ui

import (
	"time"

	"gitlab.com/onikolas/gapbuffer"
	"gitlab.com/onikolas/math"
	"gitlab.com/onikolas/agl/sprite"
)

// Shows text from a gap buffer. Usefull for inserting/editing text
type TextInput struct {
	// Can access directly to edit text. Changes will be visible on next Update.
	Text *gapbuffer.GapBuffer
	// Used to access the cursor's position. Any value edits wil be overwritten by Update.
	CursorBox *FixedSizeBox

	sprites       []sprite.Sprite
	cursorSprite  *Animation
	cursorVisible bool
	drawStart     int // index into Text to start drawing from
	fontId        string
	UI
}

// Create a new text input component. Parameters font and spriteStore can be created using text.CreateTextStore.
func NewTextInput(txt string, fontid string, cursorSprite *Animation) *TextInput {
	t := &TextInput{
		Text:          gapbuffer.NewGapBuffer(256),
		cursorSprite:  cursorSprite,
		cursorVisible: true,
		fontId:        fontid,
	}
	t.Text.InsertAtCursor([]rune(txt))
	t.CursorBox = NewFixedSizeBox(t.UI.BoundingBox.P1, math.Vector2[int]{
		X: atlasManager.textDrawers[fontid].Font.HSpace,
		Y: atlasManager.textDrawers[fontid].Font.VSpace,
	})
	t.CursorBox.SetParent(t)
	t.CursorBox.AddChild(t.cursorSprite)
	return t
}

func (a *TextInput) Update(depth int, dt time.Duration) {

	boundingBox := a.UI.Parent.BoundingBox()
	atlasManager.textDrawers[a.fontId].Area = boundingBox
	atlasManager.textDrawers[a.fontId].Depth = depth - 1
	a.sprites = a.sprites[:0]
	atlasManager.textDrawers[a.fontId].Reset()
	cursor := a.Text.Cursor()
	cursorPos := math.Vector2[int]{X: 0, Y: 0}
	a.cursorVisible = false

	if cursor < a.drawStart {
		// move the draw start index backwards but make sure its on the begining of a line
		a.drawStart = cursor
		pos, ok := a.Text.FindRuneBackwards('\n', a.drawStart-1)
		if ok {
			a.drawStart = pos + 1
		} else {
			a.drawStart = 0
		}
	} else {
		// get text to draw (will get a conservative estimate)
		maxRunes := atlasManager.textDrawers[a.fontId].MaxRunes()
		txt := a.Text.Region(a.drawStart, a.drawStart+maxRunes)

		if cursor-a.drawStart <= len(txt) {
			// draw text
			a.sprites = atlasManager.textDrawers[a.fontId].AddText(txt[:cursor-a.drawStart], a.sprites)
			cursorPos = atlasManager.textDrawers[a.fontId].GetDrawPosition()
			a.sprites = atlasManager.textDrawers[a.fontId].AddText(txt[cursor-a.drawStart:], a.sprites)

			// draw cursor if its in the area of this textinput box
			// returned cursorPos will be below the bounding box if text[:cursor-a.drawStart] did not fit on screen
			if boundingBox.Contains(cursorPos) {
				a.CursorBox.Anchor = cursorPos
				a.CursorBox.Update(depth-1, dt)
				a.cursorVisible = true
			} else {
				a.cursorVisible = false
				// move the drawing start point down
				pos, ok := a.Text.FindRune('\n', a.drawStart)
				if ok {
					a.drawStart = pos + 1
				} else {
					a.drawStart = a.Text.Length()
				}
			}
		}
	}

	for _, c := range a.Children {
		c.Update(depth-1, dt)
	}
}

func (a *TextInput) AddChild(child Element) {
	a.Children = append(a.Children, child)
	child.SetParent(a)
}

func (a *TextInput) SetParent(parent Element) {
	a.Parent = parent
}

func (a *TextInput) BoundingBox() math.Box2D[int] {
	return a.UI.BoundingBox
}

func (a *TextInput) GetChildren() []Element {
	return a.Children
}

func (a *TextInput) Draw() {
	for _, v := range a.sprites {
		v.QueueRender()
	}
	if a.cursorVisible {
		a.cursorSprite.Draw()
	}
}

func (a *TextInput) String() string {
	return "TextInput"
}

func (a *TextInput) GetName() string {
	return a.Name
}

func (a *TextInput) MoveCursorUp() {
	a.Text.MoveCursor(a.Text.Cursor() - 1)
	pos, ok := a.Text.FindRuneBackwards('\n', a.Text.Cursor())
	if ok {
		a.Text.MoveCursor(pos)
	} else {
		a.Text.MoveCursor(0)
	}
}

func (a *TextInput) MoveCursorDown() {
	pos, ok := a.Text.FindRune('\n', a.Text.Cursor())
	if ok {
		a.Text.MoveCursor(pos + 1)
	} else {
		a.Text.MoveCursor(a.Text.Length())
	}
}

func (a *TextInput) MoveCursorLeft() {
	a.Text.MoveCursor(a.Text.Cursor() - 1)
}

func (a *TextInput) MoveCursorRight() {
	a.Text.MoveCursor(a.Text.Cursor() + 1)
}

func (a *TextInput) MoveCursorLineStart() {
	a.Text.MoveCursor(a.Text.Cursor() - 1)
	pos, ok := a.Text.FindRuneBackwards('\n', a.Text.Cursor())
	if ok {
		a.Text.MoveCursor(pos + 1)
	} else {
		a.Text.MoveCursor(0)
	}
}

func (a *TextInput) MoveCursorLineEnd() {
	pos, ok := a.Text.FindRune('\n', a.Text.Cursor())
	if ok {
		a.Text.MoveCursor(pos)
	} else {
		a.Text.MoveCursor(a.Text.Length())
	}
}
