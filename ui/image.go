package ui

import (
	"time"

	"gitlab.com/onikolas/math"
	"gitlab.com/onikolas/agl/sprite"
)

// Image draws an image which fills it's parent's bounding box.
type Image struct {
	sprite sprite.Sprite
	UI
}

func NewImage(filename string) (*Image, error) {
	spriteId, err := atlasManager.AddImage(filename)
	if err != nil {
		return nil, err
	}

	sp, err := atlasManager.atlases[ImageAtlas].NewSprite(spriteId)
	if err != nil {
		return nil, err
	}
	return &Image{sprite: sp}, nil
}

func (a *Image) Update(depth int, dt time.Duration) {
	a.UI.BoundingBox = a.Parent.BoundingBox()
	a.sprite.SetPosition(a.UI.BoundingBox.P1)
	a.sprite.SetDepth(depth - 1)
	a.sprite.SetSize(a.UI.BoundingBox.Size())
	for _, c := range a.Children {
		c.Update(depth-1, dt)
	}
}

func (a *Image) AddChild(child Element) {
	a.Children = append(a.Children, child)
	child.SetParent(a)
}

func (a *Image) SetParent(parent Element) {
	a.Parent = parent
}

func (a *Image) BoundingBox() math.Box2D[int] {
	return a.UI.BoundingBox
}

func (a *Image) GetChildren() []Element {
	return a.Children
}

func (a *Image) Draw() {
	a.sprite.QueueRender()
}

func (a *Image) String() string {
	return "Image"
}

func (a *Image) GetName() string {
	return a.Name
}
