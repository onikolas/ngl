package ui

// Draws ui Element and all its children
func draw(n Element) {
	n.Draw()
	for _, v := range n.GetChildren() {
		draw(v)
	}
}
