package ui

import (
	"time"

	"gitlab.com/onikolas/math"
)

// Box with a fixed size. The box anchors to a specific point relative to its parent's lower-left bounding box.
type FixedSizeBox struct {
	Anchor math.Vector2[int]
	Size   math.Vector2[int]
	UI
}

func NewFixedSizeBox(anchor, size math.Vector2[int]) *FixedSizeBox {
	return &FixedSizeBox{
		Anchor: anchor,
		Size:   size,
	}
}

func (a *FixedSizeBox) Update(depth int, dt time.Duration) {
	a.UI.BoundingBox.P1 = a.Parent.BoundingBox().P1.Add(a.Anchor)
	a.UI.BoundingBox.P2 = a.UI.BoundingBox.P1.Add(a.Size)

	for _, c := range a.Children {
		c.Update(depth, dt)
	}
}

func (a *FixedSizeBox) AddChild(child Element) {
	a.Children = append(a.Children, child)
	child.SetParent(a)
}

func (a *FixedSizeBox) SetParent(parent Element) {
	a.Parent = parent
}

func (a *FixedSizeBox) BoundingBox() math.Box2D[int] {
	return a.UI.BoundingBox
}

func (a *FixedSizeBox) GetChildren() []Element {
	return a.Children
}

func (a *FixedSizeBox) Draw() {}

func (a *FixedSizeBox) String() string {
	return "FixedSizeBox"
}

func (a *FixedSizeBox) GetName() string {
	return a.Name
}
