package ui

import (
	"time"

	"gitlab.com/onikolas/math"
)

// Slits the parent vertically starting at Start% ant ending at End%
type VerticalSplitBox struct {
	Start, End int // Where to start and end the split (0-100%)
	UI
}

// Creates a VerticalSplitBox that spans the parent Element's Height and start:end% of it's parent's width
func NewVerticalSplitBox(start, end int) *VerticalSplitBox {
	return &VerticalSplitBox{
		Start: start,
		End:   end,
	}
}

func (a *VerticalSplitBox) Update(depth int, dt time.Duration) {
	parentBB := a.Parent.BoundingBox()
	parentSize := parentBB.Size()

	a.UI.BoundingBox.P1.X = parentBB.P1.X + a.Start*parentSize.X/100
	a.UI.BoundingBox.P2.X = parentBB.P1.X + a.End*parentSize.X/100
	a.UI.BoundingBox.P1.Y = parentBB.P1.Y
	a.UI.BoundingBox.P2.Y = parentBB.P2.Y

	for _, c := range a.Children {
		c.Update(depth, dt)
	}
}

func (a *VerticalSplitBox) AddChild(child Element) {
	a.Children = append(a.Children, child)
	child.SetParent(a)
}

func (a *VerticalSplitBox) SetParent(parent Element) {
	a.Parent = parent
}

func (a *VerticalSplitBox) BoundingBox() math.Box2D[int] {
	return a.UI.BoundingBox
}

func (a *VerticalSplitBox) GetChildren() []Element {
	return a.Children
}

func (a *VerticalSplitBox) Draw() {}

func (a *VerticalSplitBox) String() string {
	return "VerticalSplitBox"
}

func (a *VerticalSplitBox) GetName() string {
	return a.Name
}
