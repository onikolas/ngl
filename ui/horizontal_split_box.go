package ui

import (
	"time"

	"gitlab.com/onikolas/math"
)

// Slits the parent horizontaly starting at Start% ant ending at End%
type HorizontalSplitBox struct {
	Start, End int // Where to start and end the split (0-100%)
	UI
}

// Creates a HorizontalSplitBox that spans the parent Element's width and start:end% of it's parent's height
func NewHorizontalSplitBox(start, end int) *HorizontalSplitBox {
	return &HorizontalSplitBox{
		Start: start,
		End:   end,
	}
}

func (a *HorizontalSplitBox) Update(depth int, dt time.Duration) {
	parentBB := a.Parent.BoundingBox()
	parentSize := parentBB.Size()

	a.UI.BoundingBox.P1.Y = parentBB.P1.Y + a.Start*parentSize.Y/100
	a.UI.BoundingBox.P2.Y = parentBB.P1.Y + a.End*parentSize.Y/100
	a.UI.BoundingBox.P1.X = parentBB.P1.X
	a.UI.BoundingBox.P2.X = parentBB.P2.X

	for _, c := range a.Children {
		c.Update(depth, dt)
	}
}

func (a *HorizontalSplitBox) AddChild(child Element) {
	a.Children = append(a.Children, child)
	child.SetParent(a)
}

func (a *HorizontalSplitBox) SetParent(parent Element) {
	a.Parent = parent
}

func (a *HorizontalSplitBox) BoundingBox() math.Box2D[int] {
	return a.UI.BoundingBox
}

func (a *HorizontalSplitBox) GetChildren() []Element {
	return a.Children
}

func (a *HorizontalSplitBox) Draw() {}

func (a *HorizontalSplitBox) String() string {
	return "HorizontalSplitBox"
}

func (a *HorizontalSplitBox) GetName() string {
	return a.Name
}
