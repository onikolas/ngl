package ui

import (
	"fmt"
	"strings"
	"time"

	"gitlab.com/onikolas/math"
	"gitlab.com/onikolas/agl/sprite"
)

// Holds the entire UI. It's bounding box should be kept up to date with the app window's size
type MainWindow struct{ UI }

func (a *MainWindow) Update(depth int, dt time.Duration) {
	// no parent, so assumes its own bounding box has been set manually
	// update all children
	for _, c := range a.Children {
		c.Update(sprite.MAX_DEPTH, dt)
	}
}

func (a *MainWindow) AddChild(child Element) {
	a.Children = append(a.Children, child)
	child.SetParent(a)
}

func (a *MainWindow) SetParent(parent Element) {
	panic("Top level window has no parent")
}

func (a *MainWindow) BoundingBox() math.Box2D[int] {
	return a.UI.BoundingBox
}

func (a *MainWindow) GetChildren() []Element {
	return a.Children
}

func (a *MainWindow) Draw() {
	for _, child := range a.Children {
		draw(child)
	}
}

// Prints the UI tree
func (a *MainWindow) String() string {
	builder := strings.Builder{}
	builder.WriteString("Mainwindow\n")
	for _, c := range a.Children {
		toString(c, &builder, 1)
	}
	return builder.String()
}

func toString(e Element, builder *strings.Builder, depth int) {
	for i := 0; i < depth; i++ {
		builder.WriteString(".")
	}
	fmt.Fprintf(builder, "%v \n", e)
	for _, c := range e.GetChildren() {
		toString(c, builder, depth+1)
	}
}

// Prints the UI tree
func (a *MainWindow) GetName() string {
	return a.Name
}

func (a *MainWindow) Resize(dims math.Vector2[int]) {
	a.UI.BoundingBox.P2 = dims
}
