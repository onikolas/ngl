package ui

import (
	"time"

	"gitlab.com/onikolas/math"
	"gitlab.com/onikolas/agl/sprite"
	"gitlab.com/onikolas/agl/text"
)

// Shows a text string. For editable text use TextInput
type TextString struct {
	text     string
	tabWidth int

	fontId  string
	sprites []sprite.Sprite

	UI
}

// Create a text string using a registered font. A font can be registered using ui.RegisterFont
func NewTextString(txt string, fontId string) *TextString {
	if _, ok := atlasManager.textDrawers[fontId]; !ok {
		return nil
	}
	return &TextString{
		text:     txt,
		fontId:   fontId,
		tabWidth: 0,
	}
}

// Create a text string providing a font file to be used. The provided font can be reused and its fontId will be
// fontfile-fontSize
func NewTextStringFromFontFile(txt string, fontfile string, fontSize int) *TextString {
	err := atlasManager.AddFontFile(fontfile, fontSize, text.CharacterSetASCII())
	if err != nil {
		panic(err)
	}
	return &TextString{
		text:     txt,
		fontId:   fontId(fontfile, fontSize),
		tabWidth: 0,
	}
}

func (a *TextString) Update(depth int, dt time.Duration) {
	// clear and draw from scratch (TODO can optimize)
	a.sprites = a.sprites[:0]
	atlasManager.textDrawers[a.fontId].Area = a.UI.Parent.BoundingBox()
	atlasManager.textDrawers[a.fontId].Reset()
	atlasManager.textDrawers[a.fontId].Depth = depth - 1
	a.sprites = atlasManager.textDrawers[a.fontId].AddText([]rune(a.text), a.sprites)
	for _, c := range a.Children {
		c.Update(depth-1, dt)
	}
}

func (a *TextString) AddChild(child Element) {
	a.Children = append(a.Children, child)
	child.SetParent(a)
}

func (a *TextString) SetParent(parent Element) {
	a.Parent = parent
}

func (a *TextString) BoundingBox() math.Box2D[int] {
	return a.UI.BoundingBox
}

func (a *TextString) GetChildren() []Element {
	return a.Children
}

func (a *TextString) Draw() {
	for _, v := range a.sprites {
		v.QueueRender()
	}
}

func (a *TextString) String() string {
	return "TextString " + a.text
}

func (a *TextString) GetName() string {
	return a.Name
}

// Set the text of this TextString
func (a *TextString) SetText(text string) {
	a.text = text
}

func (a *TextString) SetTabWidth(tabWidth int) {
	a.tabWidth = tabWidth
}
