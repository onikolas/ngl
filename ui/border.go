package ui

import (
	"time"

	"gitlab.com/onikolas/math"
	"gitlab.com/onikolas/agl/sprite"
)

const (
	ll = iota
	lr
	ul
	ur
	left
	right
	top
	bot
)

// Draws a border around the parent bounding box using 8 sprites. The corner sprites are drawn as
// is. The edges are stetched to cover the window.
// TODO add option to repeat the edges instead of stretching.
type Border struct {
	sprites      []sprite.Sprite
	inwardOffset int // how many pixels to shift child elements inwards
	UI
}

// NewBorder from existing sprites. Provide sprite ids for four corners and vertical/horizontal sections
func NewBorder(lowerLeft, lowerRight, upperLeft, upperRight, left, right, top, bot int) *Border {

	b := &Border{
		sprites: make([]sprite.Sprite, 8),
	}

	b.sprites[ll], _ = atlasManager.atlases[ImageAtlas].NewSprite(lowerLeft)
	b.sprites[lr], _ = atlasManager.atlases[ImageAtlas].NewSprite(lowerRight)
	b.sprites[ul], _ = atlasManager.atlases[ImageAtlas].NewSprite(upperLeft)
	b.sprites[ur], _ = atlasManager.atlases[ImageAtlas].NewSprite(upperRight)
	b.sprites[left], _ = atlasManager.atlases[ImageAtlas].NewSprite(left)
	b.sprites[right], _ = atlasManager.atlases[ImageAtlas].NewSprite(right)
	b.sprites[top], _ = atlasManager.atlases[ImageAtlas].NewSprite(top)
	b.sprites[bot], _ = atlasManager.atlases[ImageAtlas].NewSprite(bot)
	return b
}

// Create a border from an existing one.
func NewBorderCopy(b *Border) *Border {
	bb := &Border{
		sprites: make([]sprite.Sprite, 8),
	}

	for i := range b.sprites {
		bb.sprites[i], _ = atlasManager.atlases[ImageAtlas].NewSprite(b.sprites[i].GetIndex())
	}

	return bb
}

// Create a border from an image. The image is split into a 3x3 grid which is used as the corner and
// edge sprites of Border (middle is ignored).
func NewBorderFromImage(image string) (*Border, error) {
	rgba, err := sprite.RgbaFromFile(image)
	if err != nil {
		return nil, err
	}
	bounds := rgba.Bounds()
	dx, dy := bounds.Dx(), bounds.Dy()
	w, h := dx/3, dy/3 //cell size
	//corners
	bb := make([]math.Box2D[int], 8)
	bb[ul] = math.Box2D[int]{}.New(0, 0, w, h)
	bb[ur] = math.Box2D[int]{}.New(2*w, 0, 3*w, h)
	bb[ll] = math.Box2D[int]{}.New(0, 2*h, w, 3*h)
	bb[lr] = math.Box2D[int]{}.New(2*w, 2*h, 3*w, 3*h)
	//edges
	bb[left] = math.Box2D[int]{}.New(0, h, w, 2*h)
	bb[right] = math.Box2D[int]{}.New(2*w, h, 3*w, 2*h)
	bb[top] = math.Box2D[int]{}.New(w, 0, 2*w, h)
	bb[bot] = math.Box2D[int]{}.New(w, 2*h, 2*w, 3*h)

	_, sprites, err := atlasManager.atlases[ImageAtlas].AddAtlasImage(rgba, bb)
	if err != nil || len(sprites) != 8 {
		return nil, err
	}

	b := &Border{
		sprites: make([]sprite.Sprite, 8),
	}
	for i := range bb {
		b.sprites[i], err = atlasManager.atlases[ImageAtlas].NewSprite(sprites[i])
		if err != nil {
			return nil, err
		}
	}
	return b, nil
}

func (a *Border) Update(depth int, dt time.Duration) {
	bb := a.Parent.BoundingBox()
	a.UI.BoundingBox = bb

	ss := a.sprites[ll].GetOriginalSize()

	a.sprites[ll].SetPosition(bb.P1)
	a.sprites[lr].SetPosition(math.Vector2[int]{X: bb.P2.X - ss.X, Y: bb.P1.Y})
	a.sprites[ur].SetPosition(bb.P2.Sub(ss))
	a.sprites[ul].SetPosition(math.Vector2[int]{X: bb.P1.X, Y: bb.P2.Y - ss.Y})

	a.sprites[left].SetPosition(math.Vector2[int]{X: bb.P1.X, Y: bb.P1.Y + ss.Y})
	a.sprites[right].SetPosition(math.Vector2[int]{X: bb.P2.X - ss.X, Y: bb.P1.Y + ss.Y})
	a.sprites[top].SetPosition(math.Vector2[int]{X: bb.P1.X + ss.X, Y: bb.P2.Y - ss.Y})
	a.sprites[bot].SetPosition(math.Vector2[int]{X: bb.P1.X + ss.X, Y: bb.P1.Y})

	a.sprites[ll].SetOriginalSize()
	a.sprites[lr].SetOriginalSize()
	a.sprites[ul].SetOriginalSize()
	a.sprites[ur].SetOriginalSize()

	a.sprites[top].SetSize(math.Vector2[int]{X: bb.Size().X - 2*ss.X, Y: ss.Y})
	a.sprites[bot].SetSize(math.Vector2[int]{X: bb.Size().X - 2*ss.X, Y: ss.Y})
	a.sprites[left].SetSize(math.Vector2[int]{X: ss.X, Y: bb.Size().Y - 2*ss.Y})
	a.sprites[right].SetSize(math.Vector2[int]{X: ss.X, Y: bb.Size().Y - 2*ss.Y})

	for i := range a.sprites {
		a.sprites[i].SetDepth(depth - 1)
	}

	for _, c := range a.Children {
		c.Update(depth-1, dt)
	}
}

func (a *Border) AddChild(child Element) {
	a.Children = append(a.Children, child)
	child.SetParent(a)
}

func (a *Border) SetParent(parent Element) {
	a.Parent = parent
}

func (a *Border) BoundingBox() math.Box2D[int] {
	// report a smaller bounding box to account for the border width
	bb := a.UI.BoundingBox
	bb.P1 = bb.P1.Add(math.Vector2[int]{a.inwardOffset, a.inwardOffset})
	bb.P2 = bb.P2.Sub(math.Vector2[int]{a.inwardOffset, a.inwardOffset})
	return bb
}

func (a *Border) GetChildren() []Element {
	return a.Children
}

func (a *Border) Draw() {
	for i := range a.sprites {
		a.sprites[i].QueueRender()
	}
}

func (a *Border) String() string {
	return "Border"
}

func (a *Border) GetName() string {
	return a.UI.Name
}

func (a *Border) SetInwardOffset(offset int) {
	a.inwardOffset = offset
}
