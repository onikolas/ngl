package ui

import (
	"time"

	"gitlab.com/onikolas/math"
)

// Children of VerticalStack are arranged vertically taking up all of the available space
// By default stacks in bottom to top order (since lower left is the origin) but this can be
// reversed.
type VerticalStack struct {
	Reverse    bool
	childCount int
	UI
}

func (a *VerticalStack) Update(depth int, dt time.Duration) {
	a.UI.BoundingBox = a.Parent.BoundingBox()
	a.childCount = 0
	for _, c := range a.Children {
		c.Update(depth, dt)
	}
}

func (a *VerticalStack) AddChild(child Element) {
	a.Children = append(a.Children, child)
	child.SetParent(a)
}

func (a *VerticalStack) SetParent(parent Element) {
	a.Parent = parent
}

// Keep track of children who have asked for a bb and give them stacked boxes inside our bb
func (a *VerticalStack) BoundingBox() math.Box2D[int] {
	numChildren := len(a.Children)
	bb := a.Parent.BoundingBox()
	h := bb.Size().Y / numChildren

	childBox := math.Box2D[int]{
		P1: math.Vector2[int]{X: bb.P1.X, Y: bb.P1.Y + a.childCount*h},
		P2: math.Vector2[int]{X: bb.P2.X, Y: bb.P1.Y + (a.childCount+1)*h},
	}
	if a.Reverse {
		childBox = math.Box2D[int]{
			P1: math.Vector2[int]{X: bb.P1.X, Y: bb.P2.Y - (a.childCount+1)*h},
			P2: math.Vector2[int]{X: bb.P2.X, Y: bb.P2.Y - a.childCount*h},
		}
	}

	a.childCount++
	return childBox
}

func (a *VerticalStack) GetChildren() []Element {
	return a.Children
}

func (a *VerticalStack) Draw() {}

func (a *VerticalStack) String() string {
	return "VerticalStack"
}

func (a *VerticalStack) GetName() string {
	return a.Name
}
