package text

// ascii printable chars
func CharacterSetASCII() []rune {
	s := make([]rune, 0, 127-32)
	for i := 32; i < 127; i++ {
		s = append(s, rune(i))
	}
	return s
}

// Greek characters
func CharacterSetGreek() []rune {
	s := make([]rune, 0, 1023-880)
	for i := 880; i < 1023; i++ {
		s = append(s, rune(i))
	}
	return s
}
