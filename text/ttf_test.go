package text

import (
	"fmt"
	"testing"

	"gitlab.com/onikolas/agl/imageutil"
)

func TestNewTrueTypeFont(t *testing.T) {
	for _, i := range []int{6, 12, 16, 20, 24, 32, 40, 64, 80} {
		font, err := NewTrueTypeFont("data/OpenSans-Regular.ttf", float64(i), CharacterSetASCII())
		if err != nil {
			t.Fatal(err)
		}

		imageutil.SaveRgba(font.image, fmt.Sprint("font-", i, ".png"))
	}

}
