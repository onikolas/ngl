package text

import (
	"gitlab.com/onikolas/agl/sprite"
	"gitlab.com/onikolas/math"
)

// Adds a font to a sprite atlas. Returns a list of runes and a matching list of sprite ids
func FontToAtlas(font Font, atlas *sprite.Atlas) ([]rune, []int, error) {
	runes := font.Runes()
	boundingBoxes := []math.Box2D[int]{}
	for _, r := range runes {
		metrics := font.RuneMetrics(r)
		metrics.BoundingBox.MakeCanonical()
		boundingBoxes = append(boundingBoxes, metrics.BoundingBox)
	}

	_, ids, err := atlas.AddAtlasImage(font.Image(), boundingBoxes)
	if err != nil {
		return nil, nil, err
	}
	return runes, ids, nil
}
