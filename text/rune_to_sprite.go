package text

// Holds runes and their corresponding sprite ids in two arrays whose indexes match.
type RuneToSpriteMap struct {
	Runes   []rune
	Sprites []int
}

func NewRuneToSpriteMap(runes []rune, sprites []int) *RuneToSpriteMap {
	m := RuneToSpriteMap{
		Runes:   make([]rune, len(runes)),
		Sprites: make([]int, len(sprites)),
	}
	copy(m.Runes, runes)
	copy(m.Sprites, sprites)
	return &m
}

// Get sprite for this rune
func (rm *RuneToSpriteMap) Get(r rune) int {
	// TODO this can be optimized to directly index e.g. when we know r is an ASCII
	// if the runes are stored predictably
	for i := range rm.Runes {
		if rm.Runes[i] == r {
			return rm.Sprites[i]
		}
	}
	return -1
}
