package text

import "gitlab.com/onikolas/math"

// RuneMetrics stores rune information: bounding box, origin and advance
type RuneMetrics struct {
	BoundingBox math.Box2D[int] //glyph fits inside this
	Adjust      math.Vector2[int]
	Advance     int
}
