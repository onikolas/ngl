package text

import (
	"image"
	"image/draw"
	stdmath "math"
	"os"

	"github.com/golang/freetype/truetype"
	"gitlab.com/onikolas/agl/imageutil"
	"gitlab.com/onikolas/math"
	"golang.org/x/image/font"
	"golang.org/x/image/math/fixed"
)

// TrueTypeFont holds an image with renders of characters from a true type font.
type TrueTypeFont struct {
	image       *image.RGBA   // image containing the glyphs of this font
	runes       []rune        // runes(characters,symbols) stored in this font
	runeMetrics []RuneMetrics // render information for each rune (matches order of runes)
	fontMetrics FontMetrics
}

func NewTrueTypeFont(fontFile string, fontSize float64, runes []rune) (*TrueTypeFont, error) {
	ttf := &TrueTypeFont{
		runes:       runes,
		fontMetrics: FontMetrics{Size: int(fontSize)},
	}

	fontBytes, err := os.ReadFile(fontFile)
	if err != nil {
		return ttf, err
	}

	f, err := truetype.Parse(fontBytes)
	if err != nil {
		return ttf, err
	}
	face := truetype.NewFace(f, &truetype.Options{
		Size:    fontSize,
		Hinting: font.HintingFull,
	})

	dim, maxAdv := faceAreaMetrics(face, runes)
	imageSize := math.Vector2[int]{X: int(dim), Y: int(dim)}
	ttf.image = image.NewRGBA(image.Rect(0, 0, imageSize.X, imageSize.Y))

	// drawing point
	yAdvance := face.Metrics().Ascent.Ceil()
	ttf.fontMetrics.YAdvance = yAdvance
	dot := fixed.P(0, yAdvance)

	for _, r := range runes {
		rec, mask, maskp, adv, ok := face.Glyph(dot, r)
		if !ok {
			continue
		}
		draw.Draw(ttf.image, rec, mask, maskp, draw.Src)

		bbox := math.Box2D[int]{}.FromImageRect(rec)
		bbox.MakeCanonical()
		dotvec := fixedPointToVec(dot)
		ttf.runeMetrics = append(ttf.runeMetrics, RuneMetrics{
			BoundingBox: bbox,
			Advance:     adv.Ceil(),
			Adjust:      bbox.P3().Sub(dotvec),
		})

		dot.X += adv
		if dot.X > fixed.I(imageSize.X-maxAdv) {
			dot.X, dot.Y = 0, dot.Y+fixed.I(yAdvance)
		}
	}

	ttf.crisp()

	return ttf, nil
}

func faceAreaMetrics(face font.Face, runes []rune) (int, int) {
	totalWidth := 0
	maxAdv := 0
	for _, r := range runes {
		_, _, _, adv, ok := face.Glyph(fixed.P(0, 0), r)
		if ok {
			if adv.Ceil() > maxAdv {
				maxAdv = adv.Ceil()
			}
			totalWidth += adv.Ceil()
		}
	}
	totalArea := totalWidth * face.Metrics().Ascent.Ceil()
	side := stdmath.Sqrt(float64(totalArea))

	return int(math.NextPowerOf2(uint64(side))), maxAdv
}

func (t *TrueTypeFont) Image() *image.RGBA {
	return t.image
}

func (t *TrueTypeFont) RuneMetrics(r rune) RuneMetrics {
	i := 0
	for i = range t.runes {
		if t.runes[i] == r {
			break
		}
	}
	if i == len(t.runeMetrics) {
		return RuneMetrics{}
	}
	return t.runeMetrics[i]
}

func fixedPointToVec(p fixed.Point26_6) math.Vector2[int] {
	return math.Vector2[int]{X: p.X.Ceil(), Y: p.Y.Ceil()}
}

func (t *TrueTypeFont) FontMetrics() FontMetrics {
	return t.fontMetrics
}

func (t *TrueTypeFont) Runes() []rune {
	return t.runes
}

func (t *TrueTypeFont) crisp() {
	t.image = imageutil.Ceil(t.image, 0.5, imageutil.RGBA_CHANEL)
}
