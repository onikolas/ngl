package text

import (
	"image"
	"testing"

	"gitlab.com/onikolas/agl/color"
	"gitlab.com/onikolas/agl/imageutil"
	"gitlab.com/onikolas/math"
)

var panagram = "How vexingly quick daft zebras jump!"

var sampletext = `I have frequently wondered
 if the majority of mankind ever pause to reflect upon
the occasionally titanic significance of dreams, and of the obscure world to which they
belong. Whilst the greater number of our nocturnal visions are perhaps no more than faint and
fantastic reflections of our waking experiences. Freud to the contrary with his puerile
symbolism, there are still a certain remainder whose immundane and ethereal character permits of no
ordinary interpretation, and whose vaguely exciting and disquieting effect suggests possible minute
glimpses into a sphere of mental existence no less important than physical life, yet separated from
that life by an all but impassable barrier.`

var symbols = "!@#$%^&**(()){}}{}[]:=,.<>?/"

func TestRenderTrueType(t *testing.T) {
	font, err := NewTrueTypeFont("data/OpenSans-Regular.ttf", 32, CharacterSetASCII())
	if err != nil {
		t.Fatal(err)
	}

	img := image.NewRGBA(image.Rect(0, 0, 1000, 700))

	box := math.Box2D[int]{}.New(100, 200, 500, 500)
	imageutil.DrawRectOutline(img, box, color.COLOR_RED.ToImageRGBA(), 1)
	Render(img, box, sampletext, font)

	box = math.Box2D[int]{}.New(-10, 0, 500, 50)
	imageutil.DrawRectOutline(img, box, color.COLOR_BLUE.ToImageRGBA(), 2)
	Render(img, box, symbols, font)

	box = math.Box2D[int]{}.New(700, 400, 11500, 11500)
	imageutil.DrawRectOutline(img, box, color.COLOR_BLACK.ToImageRGBA(), 1)
	Render(img, box, sampletext, font)

	box = math.Box2D[int]{}.New(-10, 600, 650, 11500)
	imageutil.DrawRectOutline(img, box, color.COLOR_BLACK.ToImageRGBA(), 1)
	Render(img, box, panagram, font)
	imageutil.SaveRgba(img, "render-ttf.png")

	img = image.NewRGBA(image.Rect(0, 0, 300, 100))
	box = math.Box2D[int]{}.New(0, 0, 600, 200)
	Render(img, box, panagram, font)
	imageutil.SaveRgba(img, "render-ttf2.png")

}

func TestRenderBitmap(t *testing.T) {
	charList := "!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_ `abcdefghijklmnopqrstuvwxyz{|}~�"
	font, err := NewBitmapFont("data/bitmap.png", 6, 16, []rune(charList))
	if err != nil {
		t.Fatal(err)
	}

	img := image.NewRGBA(image.Rect(0, 0, 1000, 700))

	box := math.Box2D[int]{}.New(100, 200, 500, 500)
	imageutil.DrawRectOutline(img, box, color.COLOR_RED.ToImageRGBA(), 1)
	Render(img, box, sampletext, font)

	box = math.Box2D[int]{}.New(-10, 0, 500, 50)
	imageutil.DrawRectOutline(img, box, color.COLOR_BLUE.ToImageRGBA(), 2)
	Render(img, box, symbols, font)

	box = math.Box2D[int]{}.New(700, 400, 11500, 11500)
	imageutil.DrawRectOutline(img, box, color.COLOR_BLACK.ToImageRGBA(), 1)
	Render(img, box, sampletext, font)

	box = math.Box2D[int]{}.New(-10, 600, 650, 11500)
	imageutil.DrawRectOutline(img, box, color.COLOR_BLACK.ToImageRGBA(), 1)
	Render(img, box, panagram, font)
	imageutil.SaveRgba(img, "render-bitmap.png")

	img2 := image.NewRGBA(image.Rect(0, 0, 600, 100))
	box = math.Box2D[int]{}.New(0, 0, 600, 200)
	Render(img2, box, panagram, font)
	imageutil.SaveRgba(img2, "render-bitmap2.png")
}

func TestMultiRender(t *testing.T) {
	charList := "!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_ `abcdefghijklmnopqrstuvwxyz{|}~�"
	bitmapFont, err := NewBitmapFont("data/bitmap.png", 6, 16, []rune(charList))
	if err != nil {
		t.Fatal(err)
	}

	ttfFont, err := NewTrueTypeFont("data/OpenSans-Regular.ttf", 38, CharacterSetASCII())
	if err != nil {
		t.Fatal(err)
	}

	img := image.NewRGBA(image.Rect(0, 0, 500, 200))

	box := math.Box2D[int]{}.New(0, 0, 500, 100)
	Render(img, box, "Bitmap is cool and retro... ", bitmapFont)

	box = math.Box2D[int]{}.New(0, 100, 500, 200)
	Render(img, box, "But ttf is nice and smooth!", ttfFont)

	imageutil.SaveRgba(img, "render-both.png")

}
