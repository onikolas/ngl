package text

import (
	"testing"

	"gitlab.com/onikolas/math"
)

func TestBitMapFontGetRuneBounds(t *testing.T) {
	charList := "!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_ `abcdefghijklmnopqrstuvwxyz{|}~�"
	font, err := NewBitmapFont("data/bitmap.png", 6, 16, []rune(charList))
	if err != nil {
		t.Fatal(err)
	}
	bounds, err := font.GetRuneBounds('~')
	if err != nil {
		t.Fatal(err)
	}
	expect := math.Box2D[int]{}.New(448, 192, 448+32, 192-32)
	if bounds != expect {
		t.Error(bounds)
	}
}
