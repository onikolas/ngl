package text

import (
	"errors"
	"image"

	"gitlab.com/onikolas/agl/imageutil"
	"gitlab.com/onikolas/math"
)

// BitmapFont stores an image with equally sized font characters (runes). The runes are layed out in
// a grid.
type BitmapFont struct {
	image          *image.RGBA       // image containing the glyphs of this font
	runeDimensions math.Vector2[int] // width and height of each rune
	rows, columns  int               // how many glyphs per row and column in the image

	// characters in this bitmap image, the order of this array matches the order in the image (left
	// to right, top to bot)
	runes []rune
}

// Create a new bitmap from an imageFile.  Rows and columns are the number of characters per column
// and row. glyphs are the characters in this bitmap image, the order of this array matches the
// order in the image (left to right, top to bot)
func NewBitmapFont(imageFile string, rows, columns int, runes []rune) (*BitmapFont, error) {
	var err error

	bitmap := BitmapFont{
		rows:    rows,
		columns: columns,
		runes:   runes,
	}

	if bitmap.image, err = imageutil.RgbaFromFile(imageFile); err != nil {
		return &bitmap, err
	}

	imgSize := bitmap.image.Rect.Size()
	bitmap.runeDimensions = bitmapRuneDimensions(math.Vector2[int]{imgSize.X, imgSize.Y}, rows, columns)
	return &bitmap, nil
}

// Get bounding box (in pixels) of the rune in the bitmap image
func (b *BitmapFont) GetRuneBounds(r rune) (math.Box2D[int], error) {
	bb := math.Box2D[int]{}
	index := 0
	for index = range b.runes {
		if b.runes[index] == r {
			break
		}
	}

	if index == len(b.runes) {
		return math.Box2D[int]{}, errors.New("Rune not found in bitmap")
	}

	// this assumes the index matches the order in the image
	y := index/b.columns + 1 // +1 is to move the origin to the bottom left instead to upper right
	x := index % b.columns
	rd := b.runeDimensions
	bb = bb.New(x*rd.X, y*rd.Y, (x+1)*rd.X, (y-1)*rd.Y)

	return bb, nil
}

// Calculate the rune size given image size and number of rune rows/columns. Assumes the runes are
// tightly packed without borders or  space between them
func bitmapRuneDimensions(imageDimensions math.Vector2[int], rows, columns int) math.Vector2[int] {
	return math.Vector2[int]{
		X: imageDimensions.X / columns,
		Y: imageDimensions.Y / rows,
	}
}

func (b *BitmapFont) FontMetrics() FontMetrics {
	return FontMetrics{
		Size:     math.Max(b.runeDimensions.X, b.runeDimensions.Y),
		YAdvance: b.runeDimensions.Y,
	}
}

func (b *BitmapFont) Image() *image.RGBA {
	return b.image
}

func (b *BitmapFont) RuneMetrics(r rune) RuneMetrics {
	bb, _ := b.GetRuneBounds(r)
	return RuneMetrics{
		BoundingBox: bb,
		Advance:     b.runeDimensions.X,
	}
}

func (b *BitmapFont) Runes() []rune {
	return b.runes
}
