package text

import (
	"fmt"
	"image"
	"image/draw"

	"gitlab.com/onikolas/math"
)

// Draw text on target image. Text is confined to the image location given by bounds.
func Render(target *image.RGBA, bounds math.Box2D[int], text string, font Font) {
	bounds.CropToFitIn(math.Box2D[int]{}.FromImageRect(target.Rect))
	yAdvance := font.FontMetrics().YAdvance
	startingMargin := math.Vector2[int]{X: 0, Y: yAdvance}
	start := bounds.P1.Add(startingMargin)
	fmt.Println(start, startingMargin)
	dot := start

	for _, r := range text {
		rm := font.RuneMetrics(r)
		runeBB := rm.BoundingBox
		adjustedDot := dot.Add(rm.Adjust)
		if (adjustedDot.X+runeBB.Size().X) >= bounds.P2.X || r == '\n' {
			dot.X = start.X
			dot.Y += yAdvance
			adjustedDot = dot.Add(rm.Adjust)
		}
		if r == '\n' {
			continue
		}
		if adjustedDot.Y > bounds.P2.Y {
			return
		}

		destBB := math.Box2D[int]{}.New(adjustedDot.X, adjustedDot.Y,
			adjustedDot.X+runeBB.Size().X, adjustedDot.Y-runeBB.Size().Y)
		draw.Draw(target, destBB.ToImageRect(), font.Image(), runeBB.ToImageRect().Min, draw.Src)
		dot = dot.Add(math.Vector2[int]{X: rm.Advance, Y: 0})
	}
}
