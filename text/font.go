package text

import "image"

// Font is the common interface for bitmap and ttf fonts
type Font interface {
	// Get the image representation of the font
	Image() *image.RGBA
	// Get render information for a rune
	RuneMetrics(r rune) RuneMetrics
	// Get font information
	FontMetrics() FontMetrics
	// Get the runes for which this font has glyphs
	Runes() []rune
}
