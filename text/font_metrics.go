package text

type FontMetrics struct {
	Size     int
	YAdvance int
}
