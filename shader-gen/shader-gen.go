// Utility for loading a GLSL shader file and generating a go file that has a string variable with that shader's source. The
// string can then be read by the shaders package to create the shader program.
package main

import (
	"flag"
	"fmt"
	"io/ioutil"
)

func loadShaderFromFile(filename string) ([]byte, error) {
	bytes, err := ioutil.ReadFile(filename)

	if err != nil {
		return nil, err
	}

	return bytes, nil
}

func saveShaderToGoFile(shader []byte, filename, pack, varName string) error {
	goSource := fmt.Sprintf("package %v\nvar %v = `%v ` + \"\\x00\" \n", pack, varName, string(shader))
	err := ioutil.WriteFile(filename, []byte(goSource), 0644)
	return err
}

func main() {
	var source = flag.String("source", "", "Shader source file")
	var out = flag.String("out", "", "go file to save as")
	var pack = flag.String("package", "main", "Package where the go file will reside in")
	var varName = flag.String("var", "Shader", "Variable name that holds the shader")
	flag.Parse()

	shaderSource, err := loadShaderFromFile(*source)
	if err != nil {
		panic(err)
	}

	err = saveShaderToGoFile(shaderSource, *out, *pack, *varName)
	if err != nil {
		panic(err)
	}
}
