dirs =  ./platform ./shaders ./sprite ./text ./ui ./game

build:
	go build $(dirs)

test:
	go test $(dirs)
