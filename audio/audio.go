package audio

import "github.com/veandco/go-sdl2/mix"

// Initialize the sound mixer with default params. You can call mix.OpenAudio instead if you need to
// pass specific parameters like sample rate.
func Init() error {
	if err := mix.OpenAudio(44100, mix.DEFAULT_FORMAT, 2, 4096); err != nil {
		return err
	}

	return nil
}

// Music (mp3, flac, ogg, wav). Only one music track can play at a time.
type Music struct {
	*mix.Music
}

// Load a music file. It must be unloaded with Music.Free() when done.
func LoadMusic(filename string) (Music, error) {
	m, err := mix.LoadMUS(filename)
	return Music{Music: m}, err
}

// Load a music file. It must be unloaded with Music.Free() when done.
func (m *Music) Load(filename string) error {
	mus, err := mix.LoadMUS(filename)
	if err != nil {
		return err
	}
	m.Music = mus
	return nil
}

// Load a music file. It must be unloaded with Music.Free() when done. Will not load if a music file
// is already loaded.
func (m *Music) LoadOnce(filename string) error {
	if m.IsLoaded() {
		return nil
	}
	mus, err := mix.LoadMUS(filename)
	if err != nil {
		return err
	}
	m.Music = mus
	return nil
}

func (m *Music) IsLoaded() bool {
	return m.Music != nil
}

// A sound clip. Can play multiple at the same time.
type Sound struct {
	*mix.Chunk
}

// Load a sound file. It must be unloaded with Sound.Free() when done.
func LoadSound(filename string) (Sound, error) {
	c, err := mix.LoadWAV(filename)
	return Sound{Chunk: c}, err
}

// Load a sound file. It must be unloaded with Sound.Free() when done.
func (s *Sound) Load(filename string) error {
	if s.IsLoaded() {
		s.Free()
	}
	chunk, err := mix.LoadWAV(filename)
	s.Chunk = chunk
	return err
}

// Load a sound file. It must be unloaded with Sound.Free() when done. LoadOnce will do nothing if
// a sound file is already loaded.
func (s *Sound) LoadOnce(filename string) error {
	if s.IsLoaded() {
		return nil
	}
	chunk, err := mix.LoadWAV(filename)
	s.Chunk = chunk
	return err
}

// Play the sound using the next available chanel and repeat loops times. Passing 0 will play once,
// 1 will play twice etc.
func (s *Sound) Play(loops int) {
	s.Chunk.Play(-1, loops)
}

func (s *Sound) IsLoaded() bool {
	return s.Chunk != nil
}

// Set the sound volume [0,1]
func (s *Sound) SetVolume(volume float32) {
	s.Chunk.Volume(int(volume * float32(mix.MAX_VOLUME)))
}

func CloseAudio() {
	mix.CloseAudio()
}

// Set the music volume as a percent of max volume. 0.5 is half of max and 1 is full volume.
func SetMusicVolume(volume float32) {
	mix.VolumeMusic(int(volume * float32(mix.MAX_VOLUME)))
}

// Set the sound volume for all chanels. Volume is a percent of max volume so 0.5 is half of max and
// 1 is full volume.
func SetSoundVolume(volume float32) {
	mix.Volume(-1, int(volume*float32(mix.MAX_VOLUME)))
}
