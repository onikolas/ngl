package imageutil

import "testing"

func TestColorChanel(t *testing.T) {
	a := RED_CHANEL
	if a.Is(BLUE_CHANEL) {
		t.Error("red != blue")
	}
	if !a.Is(RED_CHANEL) {
		t.Error("red == red")
	}
	a = RGB_CHANEL
	if !a.Is(RED_CHANEL) {
		t.Error("rgb == red")
	}
	if !a.Is(GREEN_CHANEL) {
		t.Error("rgb == green")
	}
	if !a.Is(BLUE_CHANEL) {
		t.Error("rgb == blue")
	}
	if a.Is(ALPHA_CHANEL) {
		t.Error("rgb != alpha")
	}
	a = RGBA_CHANEL
	if !a.Is(RED_CHANEL) {
		t.Error("rgb == red")
	}
	if !a.Is(GREEN_CHANEL) {
		t.Error("rgb == green")
	}
	if !a.Is(BLUE_CHANEL) {
		t.Error("rgb == blue")
	}
	if !a.Is(ALPHA_CHANEL) {
		t.Error("rgb != alpha")
	}
}
