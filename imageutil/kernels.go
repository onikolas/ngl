package imageutil

var KernelGaussian3x3 = [][]float32{
	{0.0625, 0.1250, 0.0625},
	{0.1250, 0.2500, 0.1250},
	{0.0625, 0.1250, 0.0625},
}

var KernelLaplacian = [][]float32{
	{0.0, 1.0, 0.0},
	{1.0, -4.0, 1.0},
	{0.0, 1.0, 0.0},
}

var KernelXAngle = [][]float32{
	{0, 0, 0},
	{-1, 0, 1},
	{0, 0, 0},
}

var KernelYAngle = [][]float32{
	{0, -1, 0},
	{0, 0, 0},
	{0, 1, 0},
}
