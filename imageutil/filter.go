package imageutil

import (
	"image"
	"image/color"

	acol "gitlab.com/onikolas/agl/color"
	"gitlab.com/onikolas/math"
)

// Apply kernel to image. Kernel dimensions should be odd numbers
func Filter(img *image.RGBA, kernel [][]float32, chanels Chanel) *image.RGBA {
	filteredImage := image.NewRGBA(img.Bounds())
	imageSize := img.Bounds().Size()
	for y := 0; y < imageSize.Y; y++ {
		for x := 0; x < imageSize.X; x++ {
			p := filterAtPoint(img, kernel, math.Vector2[int]{x, y}, chanels)
			filteredImage.Set(x, y, p)
		}
	}
	return filteredImage
}

func filterAtPoint(image *image.RGBA, kernel [][]float32, p math.Vector2[int], ch Chanel) color.RGBA {
	kx := len(kernel[0])
	ky := len(kernel)
	imageBox := math.Box2D[int]{}.New(0, 0, image.Bounds().Size().X, image.Bounds().Size().Y)

	var r, g, b, a float32

	for i := -ky / 2; i <= ky/2; i++ {
		for j := -kx / 2; j <= kx/2; j++ {
			pos := p.Add(math.Vector2[int]{X: j, Y: i})
			pixel := acol.ColorRGBA{}
			if imageBox.Contains(pos) {
				pixel = acol.NewColorRGBRFromImageColor(image.At(pos.X, pos.Y))
			}
			r += kernel[i+ky/2][j+ky/2] * pixel.R
			g += kernel[i+ky/2][j+ky/2] * pixel.G
			b += kernel[i+ky/2][j+ky/2] * pixel.B
			a += kernel[i+ky/2][j+ky/2] * pixel.A
		}
	}
	unmodified := acol.NewColorRGBRFromImageColor(image.At(p.X, p.Y))
	if !ch.Is(RED_CHANEL) {
		r = unmodified.R
	}
	if !ch.Is(GREEN_CHANEL) {
		g = unmodified.G
	}
	if !ch.Is(BLUE_CHANEL) {
		b = unmodified.B
	}
	if !ch.Is(ALPHA_CHANEL) {
		a = unmodified.A
	}

	c := acol.NewColorRGBA(r, g, b, a)
	c.Clamp()
	return c.ToImageRGBA()
}

// Apply provided function to every pixel of img
func ApplyFn(img *image.RGBA, fn func(float32) float32, chanel Chanel) *image.RGBA {
	imageSize := img.Bounds().Size()
	newimg := image.NewRGBA(img.Bounds())
	for y := 0; y < imageSize.Y; y++ {
		for x := 0; x < imageSize.X; x++ {
			pixel := acol.NewColorRGBRFromImageColor(img.At(x, y))
			if chanel.Is(RED_CHANEL) {
				pixel.R = fn(pixel.R)
			}
			if chanel.Is(GREEN_CHANEL) {
				pixel.G = fn(pixel.G)
			}
			if chanel.Is(BLUE_CHANEL) {
				pixel.B = fn(pixel.B)
			}
			if chanel.Is(ALPHA_CHANEL) {
				pixel.A = fn(pixel.A)
			}
			pixel.Clamp()
			newimg.Set(x, y, pixel.ToImageRGBA())
		}
	}
	return newimg
}

// Floor the image by setting all pixels less that threshold to zero
func Floor(img *image.RGBA, threshold float32, chanels Chanel) *image.RGBA {
	fn := func(val float32) float32 {
		if val < threshold {
			return 0
		}
		return val
	}
	return ApplyFn(img, fn, chanels)
}

// Ceil the image by setting all pixels greater than threshold to 100%
func Ceil(img *image.RGBA, threshold float32, chanels Chanel) *image.RGBA {
	fn := func(val float32) float32 {
		if val > threshold {
			return 1
		}
		return val
	}
	return ApplyFn(img, fn, chanels)
}

// Multiply image pixels by factor f
func Scale(img *image.RGBA, f float32, chanels Chanel) *image.RGBA {
	fn := func(val float32) float32 {
		return val * f
	}
	return ApplyFn(img, fn, chanels)
}
