package imageutil

type Chanel uint8 // red, green, blue and alpha

const (
	RED_CHANEL   Chanel = 1
	GREEN_CHANEL Chanel = 2
	BLUE_CHANEL  Chanel = 4
	ALPHA_CHANEL Chanel = 8
	RGB_CHANEL   Chanel = RED_CHANEL | GREEN_CHANEL | BLUE_CHANEL
	RGBA_CHANEL  Chanel = RED_CHANEL | GREEN_CHANEL | BLUE_CHANEL | ALPHA_CHANEL
)

func (a Chanel) Is(b Chanel) bool {
	return a&b > 0
}
