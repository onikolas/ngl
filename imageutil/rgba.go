package imageutil

import (
	"bufio"
	"fmt"
	"image"
	"image/draw"
	"image/png"
	"os"
)

// Load an image file (only pngs for now)
func RgbaFromFile(file string) (*image.RGBA, error) {
	imgFile, err := os.Open(file)
	if err != nil {
		return nil, fmt.Errorf("texture %q not found on disk: %v", file, err)
	}
	img, _, err := image.Decode(imgFile)
	if err != nil {
		return nil, err
	}

	rgba := image.NewRGBA(img.Bounds())
	if rgba.Stride != rgba.Rect.Size().X*4 {
		return nil, fmt.Errorf("unsupported stride")
	}
	draw.Draw(rgba, rgba.Bounds(), img, image.Point{0, 0}, draw.Src)
	return rgba, nil
}

// Save rgba image to disk. Saved as png.
func SaveRgba(image *image.RGBA, filename string) error {
	outFile, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer outFile.Close()

	b := bufio.NewWriter(outFile)

	if err := png.Encode(b, image); err != nil {
		return err
	}
	if err = b.Flush(); err != nil {
		return err
	}

	return nil
}
