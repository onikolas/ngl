package imageutil

import (
	"image"
	"image/color"

	m "math"

	"gitlab.com/onikolas/math"
)

// Draw the line
func DrawLine(img *image.RGBA, p1, p2 math.Vector2[int], c color.RGBA, thickness int) {
	points := math.Line(p1, p2)

	for i := -thickness / 2; i < int(m.Ceil(float64(thickness)/2.0)); i++ {
		if i == 0 {
			continue
		}
		points = append(points, math.Line(p1.Add(math.Vector2[int]{i, 0}), p2.Add(math.Vector2[int]{i, 0}))...)
		points = append(points, math.Line(p1.Add(math.Vector2[int]{0, i}), p2.Add(math.Vector2[int]{0, i}))...)
	}

	for _, p := range points {
		img.Set(p.X, p.Y, c)
	}
}

// Draw a rectangular outline
func DrawRectOutline(img *image.RGBA, rect math.Box2D[int], c color.RGBA, thickness int) {
	rect.MakeCanonical()
	ul := math.Vector2[int]{rect.P1.X, rect.P2.Y}
	br := math.Vector2[int]{rect.P2.X, rect.P1.Y}

	DrawLine(img, rect.P1, ul, c, thickness)
	DrawLine(img, rect.P1, br, c, thickness)
	DrawLine(img, rect.P2, ul, c, thickness)
	DrawLine(img, rect.P2, br, c, thickness)
}

func DrawPoint(img *image.RGBA, point math.Vector2[int], c color.RGBA, size int) {
	for i := -size / 2; i < int(m.Ceil(float64(size)/2.0)); i++ {
		for j := -size / 2; j < int(m.Ceil(float64(size)/2.0)); j++ {
			img.Set(point.X+j, point.Y+i, c)
		}
	}
}
