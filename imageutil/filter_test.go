package imageutil

import "testing"

func TestFilter(t *testing.T) {
	image, err := RgbaFromFile("data/gopher.png")
	if err != nil {
		t.Fatal(err)
	}

	filtered := Filter(image, KernelGaussian3x3, RGB_CHANEL)
	SaveRgba(filtered, "test-gaussian.png")

	filtered = Filter(image, KernelYAngle, RGB_CHANEL)
	SaveRgba(filtered, "test-Yangle.png")

	filtered = Filter(image, KernelLaplacian, RGB_CHANEL)
	SaveRgba(filtered, "test-Laplace.png")
}

func TestScale(t *testing.T) {
	image, err := RgbaFromFile("data/gopher.png")
	if err != nil {
		t.Fatal(err)
	}

	scaled := Scale(image, 1, RGB_CHANEL)

	scaled = Scale(image, 1.2, RGB_CHANEL)
	SaveRgba(scaled, "scaled-1.2.png")

	scaled = Scale(image, 1.4,  RGB_CHANEL)
	SaveRgba(scaled, "scaled-1.4.png")

	scaled = Scale(image, 1.8, RGB_CHANEL)
	SaveRgba(scaled, "scaled-1.8.png")

	scaled = Scale(image, 0.5, RGB_CHANEL)
	SaveRgba(scaled, "scaled-0.5.png")

	scaled = Scale(image, 0.5, RGBA_CHANEL)
	SaveRgba(scaled, "scaled-alpha.png")
}

func TestFloor(t *testing.T) {
	image, err := RgbaFromFile("data/gopher.png")
	if err != nil {
		t.Fatal(err)
	}

	floored := Floor(image, 0.5, RGB_CHANEL)
	SaveRgba(floored, "floor-0.5.png")
}

func TestCeil(t *testing.T) {
	image, err := RgbaFromFile("data/gopher.png")
	if err != nil {
		t.Fatal(err)
	}

	ceiled := Ceil(image, 0.5, RGB_CHANEL)
	SaveRgba(ceiled, "ceiled-0.5.png")
}
