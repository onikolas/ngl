// Color utilities.
package color

import (
	"image/color"

	"gitlab.com/onikolas/math"
)

// Holds red, green, blue, alpha values. Each should be clamped in [0,1]. A=1 is fully opaque.
type ColorRGBA struct {
	R, G, B, A float32
}

func NewColorRGBA(r, g, b, a float32) ColorRGBA {
	return ColorRGBA{
		R: r, G: g, B: b, A: a,
	}
}

func NewColorRGBAFromArray(color [4]float32) ColorRGBA {
	return ColorRGBA{
		R: color[0], G: color[1], B: color[2], A: color[3],
	}
}

func NewColorRGBAFromSlice(color []float32) ColorRGBA {
	if len(color) != 4 {
		return COLOR_MAGENDA
	}

	return ColorRGBA{
		R: color[0], G: color[1], B: color[2], A: color[3],
	}
}

func NewColorRGBAFromBytes(r, g, b, a uint8) ColorRGBA {
	return ColorRGBA{
		R: float32(r) / 255,
		G: float32(g) / 255,
		B: float32(b) / 255,
		A: float32(a) / 255,
	}
}

func NewColorRGBRFromImageColor(c color.Color) ColorRGBA {
	r, g, b, a := c.RGBA()
	return ColorRGBA{
		R: float32(r) / 65535,
		G: float32(g) / 65535,
		B: float32(b) / 65535,
		A: float32(a) / 65535,
	}
}

// Make sure each component is in [0,1]
func (c *ColorRGBA) Clamp() {
	c.R = math.Bound(c.R, 0, 1)
	c.G = math.Bound(c.G, 0, 1)
	c.B = math.Bound(c.B, 0, 1)
	c.A = math.Bound(c.A, 0, 1)
}

func (c *ColorRGBA) ToArray() [4]float32 {
	return [4]float32{c.R, c.G, c.B, c.A}
}

func (c *ColorRGBA) ToImageRGBA() color.RGBA {
	return color.RGBA{
		math.Bound(uint8(c.R*255), 0, 255),
		math.Bound(uint8(c.G*255), 0, 255),
		math.Bound(uint8(c.B*255), 0, 255),
		math.Bound(uint8(c.A*255), 0, 255),
	}
}

func (c *ColorRGBA) Scale(r, g, b, a float32) {
	c.R *= r
	c.G *= g
	c.B *= b
	c.A *= a
}
