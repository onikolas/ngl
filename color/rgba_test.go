package color

import "testing"

func TestColorRGBAClamp(t *testing.T) {
	c := NewColorRGBA(12, 2, 0, -1)
	c.Clamp()
	cd := NewColorRGBAFromArray([4]float32{1, 1, 0, 0})
	if c != cd {
		t.Error(c, cd)
	}
}
