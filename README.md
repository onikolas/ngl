# AGL

A 2D game library built with Go, OpenGL and SDL. Tutorial and documentation can be found of
[here](https://nik-os.com/agl/00_intro.html).
