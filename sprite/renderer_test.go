package sprite

import "testing"

func TestRendererAddSpriteStore(t *testing.T) {
	stores := []*BufferList{
		{RenderOrder: 1},
		{RenderOrder: 2},
		{RenderOrder: 1},
		{RenderOrder: 3},
		{RenderOrder: 2},
		{RenderOrder: 5},
		{RenderOrder: -1},
		{RenderOrder: 3},
	}

	renderer := Renderer{}

	for _, v := range stores {
		renderer.AddBufferList(v)
	}

	expect := []int{-1, 1, 1, 2, 2, 3, 3, 5}
	for i := range renderer.buffers {
		if renderer.buffers[i].RenderOrder != expect[i] {
			t.Error("Order missmatch")
			for i := range stores {
				t.Log(stores[i].RenderOrder)
			}
			t.FailNow()
		}
	}
}
