package sprite

import (
	"errors"
	"fmt"
	"image"
	"image/color"
	"image/draw"
	"image/png"
	"os"

	"gitlab.com/onikolas/agl/imageutil"
	"gitlab.com/onikolas/math"
)

// Atlas stores a sprite atlas image and the corresponding gpu texture. It contains uv
// coordinates for the sprites it contains and operations to add sprites from images.
type Atlas struct {
	// atlas
	atlasImage          *image.RGBA
	coverageImage       *image.Gray
	spriteBoundingBoxes []math.Box2D[int]
	atlasTexture        uint32 // OpenGL texture of Atlas
}

// NewSpriteAtlas initialises a sprite atlas given an atlas image. It also allocates the GPU texture
// for the altas (it depends on OpenGl being initialized). Param spriteBoundingBoxes is a list of
// bounding boxes in the atlas. Bounding boxes are defined as point pairs that specify the lower
// left and upper right corners of each sprite. Bounding boxes are given in pixels.
func NewAtlas(atlasImage *image.RGBA, spriteBoundingBoxes []math.Box2D[int]) (*Atlas, error) {

	if !OpenGLInitialized {
		return &Atlas{}, errors.New("OpenGL not initialized")
	}

	st := Atlas{
		atlasImage:          atlasImage,
		spriteBoundingBoxes: spriteBoundingBoxes,
	}

	var err error
	if st.atlasTexture, err = textureFromRGBA(st.atlasImage); err != nil {
		return nil, err
	}

	return &st, nil
}

// Initialize an empty atlas of given size
func NewEmptyAtlas(width, height int) (*Atlas, error) {
	return NewAtlas(image.NewRGBA(image.Rect(0, 0, width, height)), nil)
}

// Initialize from an image file and a JSON encoded list of bounding boxes.
func NewAtlasFromFiles(atlasImage, spriteBoundingBoxes string) (*Atlas, error) {

	bboxes := []math.Box2D[int]{}
	err := unmarshalFile(&bboxes, spriteBoundingBoxes)
	if err != nil {
		return nil, err
	}
	img, err := imageutil.RgbaFromFile(atlasImage)
	if err != nil {
		return nil, err
	}
	return NewAtlas(img, bboxes)
}

// Get UVs for a sprite. Normalizes and returns the bounding box coordinates in a single 4-vector.
func (s *Atlas) GetSpriteUVs(index int) [4]float32 {
	box := s.spriteBoundingBoxes[index]
	img := s.atlasImage

	return [4]float32{
		// y min/max is flipped because image origin is top left but opengl is bottom left
		float32(box.P1.X) / float32(img.Rect.Dx()),
		float32(box.P2.Y) / float32(img.Rect.Dy()),
		float32(box.P2.X) / float32(img.Rect.Dx()),
		float32(box.P1.Y) / float32(img.Rect.Dy()),
	}
}

// Create  and uv buffers for a sprite
func (s *Atlas) GenerateSpriteUVs(index int) (uvs []float32) {
	box := s.spriteBoundingBoxes[index]
	img := s.atlasImage
	uv_min_x := float32(box.P1.X) / float32(img.Rect.Dx())
	uv_min_y := float32(box.P2.Y) / float32(img.Rect.Dy())
	uv_max_x := float32(box.P2.X) / float32(img.Rect.Dx())
	uv_max_y := float32(box.P1.Y) / float32(img.Rect.Dy())

	uvs = make([]float32, 12)
	uvs[0], uvs[1] = uv_min_x, uv_min_y
	uvs[2], uvs[3] = uv_max_x, uv_min_y
	uvs[4], uvs[5] = uv_min_x, uv_max_y
	uvs[6], uvs[7] = uv_min_x, uv_max_y
	uvs[8], uvs[9] = uv_max_x, uv_min_y
	uvs[10], uvs[11] = uv_max_x, uv_max_y

	return
}

// Update atlas image on gpu
func (s *Atlas) UpdateGPUAtlas() error {
	if err := updateTexture(s.atlasTexture, s.atlasImage); err != nil {
		return err
	}
	return nil
}

func (s *Atlas) addImage(img *image.RGBA) (int, error) {
	if s.coverageImage == nil {
		s.coverage()
	}

	white := color.Gray{255}
	p1 := image.Point{-1, -1}

	// find an empty spot on the atlas
	for y := 0; y < s.coverageImage.Bounds().Max.Y; y++ {
		done := false
		for x := 0; x < s.coverageImage.Bounds().Max.X; x++ {
			if s.coverageImage.At(x, y) == white {
				continue
			}
			if s.checkCoverageSpot(x, y, img.Bounds().Dx(), img.Bounds().Dy()) {
				p1 = image.Point{x, y}
				done = true
				break
			}
		}
		if done {
			break
		}
	}

	if p1.X == -1 {
		//TODO resize and don't error
		return -1, ErrorAtlasFull
	}

	// copy img to atlas and create an entry in spriteBoundingBoxes
	p2 := p1.Add(img.Rect.Max)
	rect := image.Rectangle{p1, p2}
	draw.Draw(s.atlasImage, rect, img, image.Point{}, draw.Src)
	draw.Draw(s.coverageImage, rect, image.NewUniform(white), image.Point{}, draw.Src)
	s.spriteBoundingBoxes = append(s.spriteBoundingBoxes, math.Box2D[int]{
		P1: math.Vector2[int]{p1.X, p1.Y}, P2: math.Vector2[int]{p2.X, p2.Y},
	})

	return len(s.spriteBoundingBoxes) - 1, nil
}

// Add an image to the atlas, if there is space. Returns the index for the sprite created out of img.
func (a *Atlas) AddImage(img *image.RGBA) (int, error) {
	i, err := a.addImage(img)
	if err != nil {
		return -1, err
	}
	a.UpdateGPUAtlas()
	return i, nil
}

// Add a list of images to the atlas. Returns a list of indices for the sprites added. AddImages is
// faster than calling AddImage repeatedly because it only triggers the GPU texture update  when
// all sprites have been added.
func (a *Atlas) AddImages(imgs []*image.RGBA) ([]int, error) {
	indices := []int{}
	var err error
	var i int
	for _, img := range imgs {
		i, err = a.addImage(img)
		if err != nil {
			break
		}
		indices = append(indices, i)
	}
	// will update even on error as some sprites may have been added
	a.UpdateGPUAtlas()
	return indices, err
}

// Load a png image from disk and add it to the atlas.
func (a *Atlas) AddImageFromFile(filename string) (int, error) {
	img, err := imageutil.RgbaFromFile(filename)
	if err != nil {
		return 0, err
	}
	return a.AddImage(img)
}

// Load pngs from disk and add them to the atlas.
func (a *Atlas) AddImagesFromFiles(filenames []string) ([]int, error) {
	spriteImages := []*image.RGBA{}
	for i := range filenames {
		img, err := imageutil.RgbaFromFile(filenames[i])
		if err != nil {
			return []int{}, err
		}
		spriteImages = append(spriteImages, img)
	}
	return a.AddImages(spriteImages)
}

// Add an image which is segmented into sprites. The existing sprite bounding boxes will be
// translated to the position where the image is placed. Returns the bounding box of the whole
// atlas and the individual bboxes for each sprite.
func (s *Atlas) AddAtlasImage(img *image.RGBA, spriteBBoxes []math.Box2D[int]) (int, []int, error) {
	index, err := s.AddImage(img)
	if err != nil {
		return -1, nil, err
	}

	atlasBBox := math.Box2D[int]{}.FromImageRect(s.atlasImage.Rect)
	newBBox := s.spriteBoundingBoxes[index]

	newBBoxes := []math.Box2D[int]{}

	for i := range spriteBBoxes {
		newBBox := math.Box2D[int]{
			P1: spriteBBoxes[i].P1.Add(newBBox.P1),
			P2: spriteBBoxes[i].P2.Add(newBBox.P1),
		}

		// check if valid
		if !atlasBBox.Contains(newBBox.P1) || !atlasBBox.Contains(newBBox.P2) {
			// TODO: mark coverageImage empty at newBBox
			return -1, nil, errors.New("Incorrect bounding boxes (image was added!)")
		}
		newBBoxes = append(newBBoxes, newBBox)
	}

	s.spriteBoundingBoxes = append(s.spriteBoundingBoxes, newBBoxes...)
	indices := []int{}
	for i := index + 1; i < len(s.spriteBoundingBoxes); i++ {
		indices = append(indices, i)
	}
	return index, indices, nil
}

// Same as AddAtlasImage but data is loaded from files.
func (s *Atlas) AddAtlasImageFromFiles(img, spriteBBoxes string) (int, []int, error) {
	bboxes := []math.Box2D[int]{}
	err := unmarshalFile(&bboxes, spriteBBoxes)
	if err != nil {
		return -1, nil, err
	}
	atlasImg, err := imageutil.RgbaFromFile(img)
	if err != nil {
		return -1, nil, err
	}
	return s.AddAtlasImage(atlasImg, bboxes)
}

// Add a fixed color image. Useful for debugging.
func (s *Atlas) AddFixedColorImage(color color.RGBA, size math.Vector2[int]) (int, error) {
	m := image.NewRGBA(image.Rect(0, 0, size.X, size.Y))
	draw.Draw(m, m.Bounds(), image.NewUniform(color), image.Point{}, draw.Src)
	return s.addImage(m)
}

// Get the bounding box of a sprite
func (s *Atlas) GetBoundingBox(spriteId int) math.Box2D[int] {
	if spriteId >= 0 && spriteId < len(s.spriteBoundingBoxes) {
		return s.spriteBoundingBoxes[spriteId]
	}
	return math.Box2D[int]{}
}

// Add a bounding box for an atlas. Because Atlas can contain multiple atlases, the id of the
// bounding box of the atlas must be given. The passed bounding box will be translated to the correct
// position. If the SpriteAtlas contains only one atlas, parentAtlas can be set to -1 and in which
// case translation occurs. Returns the index of the created bounding box.
func (s *Atlas) AddBoundingBox(parentAtlas int, bbox math.Box2D[int]) int {
	parentBox := s.GetBoundingBox(parentAtlas)
	bbox.P1 = bbox.P1.Add(parentBox.P1)
	bbox.P2 = bbox.P2.Add(parentBox.P1)

	s.spriteBoundingBoxes = append(s.spriteBoundingBoxes, bbox)
	return len(s.spriteBoundingBoxes) - 1
}

// Loads a list of bounding boxes from a JSON file and adds them to the sprite store using AddBoundingBox.
func (s *Atlas) AddBoundingBoxesFromFile(parentAtlas int, bbox string) []int {
	bboxes := []math.Box2D[int]{}
	if err := unmarshalFile(&bboxes, bbox); err != nil {
		return []int{}
	}

	ids := []int{}

	for i := range bboxes {
		index := s.AddBoundingBox(parentAtlas, bboxes[i])
		ids = append(ids, index)
	}
	return ids
}

// Create a coverage image. Shows available spots in the atlas as black.
func (s *Atlas) coverage() {
	if s.atlasImage == nil {
		return
	}
	if s.coverageImage == nil {
		s.coverageImage = image.NewGray(s.atlasImage.Rect)
	}

	white := color.Gray{255}

	for _, v := range s.spriteBoundingBoxes {
		for y := v.P1.Y; y < v.P2.Y; y++ {
			for x := v.P1.X; x < v.P2.X; x++ {
				s.coverageImage.SetGray(x, y, white)
			}
		}
	}
}

// check if coverage spot is empty
func (s *Atlas) checkCoverageSpot(x, y, w, h int) bool {
	if y+h >= s.coverageImage.Rect.Dy() || x+w >= s.coverageImage.Rect.Dx() {
		return false // doesn't fit
	}

	box := math.Box2D[int]{}.New(x, y, x+w, y+h)
	for _, b := range s.spriteBoundingBoxes {
		if b.Overlaps(box) {
			return false
		}
	}

	return true
}

func (s *Atlas) DumpAtlas() {
	file, _ := os.Create("atlas-image.png")
	png.Encode(file, s.atlasImage)
	file.Close()

	if s.coverageImage != nil {
		file, _ = os.Create("atlas-coverage.png")
		png.Encode(file, s.coverageImage)
		file.Close()
	}

	marshalFile(s.spriteBoundingBoxes, "bboxes.json", true)
}

// Delete should be called when the atlas is no longer needed. Handles deleting the gpu texture that
// is not garbage collected.
func (s *Atlas) Delete() {
	deleteTexture(s.atlasTexture)
}

// Clears the atlas so it can be reused for new sprites.
func (s *Atlas) Clear() {
	w, h := s.atlasImage.Rect.Dx(), s.atlasImage.Rect.Dy()
	s.atlasImage = image.NewRGBA(image.Rect(0, 0, w, h))
	s.coverageImage = nil
	s.spriteBoundingBoxes = []math.Box2D[int]{}
}

type SpriteAtlasError int

func (s SpriteAtlasError) Error() string {
	return fmt.Sprint(int(s))
}

//go:generate stringer -type=SpriteAtlasError
const (
	ErrorSpriteLimitReached SpriteAtlasError = iota
	ErrorAtlasFull
	ErrorMissingSpriteId
)
