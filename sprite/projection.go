package sprite

// Ortho creates an orthographic projection matrix
func Ortho(left, right, bottom, top, zNear, zFar float32) [16]float32 {
	return [16]float32{
		// diagonal (scale)
		2.0 / (right - left), 0, 0, 0,
		0, 2.0 / (top - bottom), 0, 0,
		0, 0, -2.0 / (zFar - zNear), 0,
		// translate column
		-(right + left) / (right - left),
		-(top + bottom) / (top - bottom),
		-(zFar + zNear) / (zFar - zNear), 1,
	}
}
