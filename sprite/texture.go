package sprite

import (
	"fmt"
	"image"
	_ "image/png"

	"gitlab.com/onikolas/agl/imageutil"

	"github.com/go-gl/gl/v4.6-core/gl"
)

func textureFromFile(file string) (uint32, error) {
	rgba, err := imageutil.RgbaFromFile(file)
	if err != nil {
		return 0, err
	}
	return textureFromRGBA(rgba)
}

// Creates an OpenGL texture from an image.RGBA
func textureFromRGBA(img *image.RGBA) (uint32, error) {

	if img == nil {
		return 0, fmt.Errorf("nil image")
	}

	if img.Stride != img.Rect.Size().X*4 {
		return 0, fmt.Errorf("unsupported stride")
	}

	var texture uint32
	gl.GenTextures(1, &texture)
	gl.ActiveTexture(gl.TEXTURE0)
	gl.BindTexture(gl.TEXTURE_2D, texture)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST)
	//gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE)
	//gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE)
	gl.TexImage2D(
		gl.TEXTURE_2D,
		0,
		gl.RGBA,
		int32(img.Rect.Size().X),
		int32(img.Rect.Size().Y),
		0,
		gl.RGBA,
		gl.UNSIGNED_BYTE,
		gl.Ptr(img.Pix))

	return texture, GetGLError()
}

func updateTexture(texture uint32, img *image.RGBA) error {
	if img == nil {
		return fmt.Errorf("nil image")
	}
	if img.Stride != img.Rect.Size().X*4 {
		return fmt.Errorf("unsupported stride")
	}
	gl.BindTexture(gl.TEXTURE_2D, texture)
	gl.TexImage2D(
		gl.TEXTURE_2D,
		0,
		gl.RGBA,
		int32(img.Rect.Size().X),
		int32(img.Rect.Size().Y),
		0,
		gl.RGBA,
		gl.UNSIGNED_BYTE,
		gl.Ptr(img.Pix))

	return GetGLError()
}

func deleteTexture(texture uint32) {
	t := []uint32{texture}
	gl.DeleteTextures(1, &t[0])
}
