package sprite

import (
	"errors"

	"github.com/go-gl/gl/v4.6-core/gl"
	"gitlab.com/onikolas/agl/shaders"
	"gitlab.com/onikolas/ds"
)

var DefaultBufferSize = 1024 // automatically allocated buffers can hold this number of sprites

// BufferList is the cpu and gpu buffers needed to render with a shader. Cpu code updates the cpu
// buffers as many times as needed typically to add sprites for render. Cpu buffers are copied to
// the gpu in one go (typically once per frame). The buffers are preallocated to support a fixed
// amount of sprites (MaxSprites).
//
// RenderOrder controls the order in which this buffer will be rendered. Although a Sprite's depth
// value controls its back-to-front order, renderOrder is important for transparent sprites. For
// transparency to work, opaque sprites need to be drawn before transparent ones and its up to the
// user to enforce this by drawing separating opaque and tranparent sprites into different buffers
// and setting renderOrder so that opaque sprites are drawn first.  For cutout sprites (that only
// have 255 and 0 alpha values) the renderOrder can be ignored using this method
// https://www.khronos.org/opengl/wiki/Transparency_Sorting (up to the shader to implement this).
//
// If the render order is rigid and known beforehand, you can disable depth testing in Renderer and
// rely on render order. This could result in a performance boost (benchmarking needed).
type BufferList struct {
	RenderOrder int
	Shader      shaders.Shader
	CpuBuffers  [][]float32
	GpuBuffers  []uint32
	TypeSize    []int32 // fast access to Shader.Attributes[...].Type.Size
	NumSprites  int
	MaxSprites  int
	Texture     uint32
	VAO         uint32
}

// Create a BufferList used to render using a specific shader.
func NewBufferList(shader shaders.Shader, maxSprites, renderOrder int, texture uint32) (*BufferList, error) {
	sb := BufferList{
		RenderOrder: renderOrder,
		Shader:      shader,
		CpuBuffers:  make([][]float32, len(shader.Attributes)-1),
		GpuBuffers:  make([]uint32, len(shader.Attributes)-1),
		MaxSprites:  maxSprites,
		NumSprites:  0,
		Texture:     texture,
	}

	// same sorting as sprite.shaderData
	attrKeys := ds.SortMapByValue(shader.Attributes, func(a, b shaders.ShaderAttribute) bool {
		return a.Location < b.Location
	})

	gl.GenVertexArrays(1, &sb.VAO)
	gl.BindVertexArray(sb.VAO)

	for i, v := range attrKeys {
		attr := shader.Attributes[v]
		if attr.Name == "vertex" {
			createSpriteVertexBuffer()
			continue
		}
		// intialize empty cpu buffers of fixed size
		sb.CpuBuffers[i-1] = make([]float32, maxSprites*int(attr.Type.Size))
		// initialize  GPU buffers
		sb.GpuBuffers[i-1] = newGLBuffer(maxSprites*int(attr.Type.Size), int(attr.Type.Size), attr.Location)
		sb.TypeSize = append(sb.TypeSize, attr.Type.Size)
	}

	gl.BindVertexArray(0)

	return &sb, nil
}

// Adds a sprite to the cpu buffer.
func (bf *BufferList) AddSprite(sprite *Sprite) error {
	if bf.NumSprites == bf.MaxSprites {
		return errors.New("Store full")
	}

	for i := range bf.CpuBuffers {
		bf.UpdateBuffers(i, bf.NumSprites, sprite.shaderData[i].Data)
	}
	bf.NumSprites++
	return nil
}

// Update the data of a buffer. Can be used to add new data as well (since the buffers are
// preallocated) The index corresponds to the sprite number and is multiplied by the size (in
// floats) of the type of this buffer (given by Shader.Attributes[bufferName].Type.Size). Typically
// one would pass data in multiples of this size but this is not enforced.
func (sb *BufferList) UpdateBuffers(buffer, index int, data []float32) {
	typeSize := int(sb.TypeSize[buffer])
	startIndex := index * typeSize
	copy(sb.CpuBuffers[buffer][startIndex:startIndex+len(data)], data)
}

// Clear all sprites from buffers
func (sb *BufferList) Empty() {
	sb.NumSprites = 0
}

// Copy all data from cpu buffers to gpu. Returns the number of floats moved.
func (bf *BufferList) MoveCpuToGpu() int {
	dataMoved := 0
	if bf.NumSprites <= 0 {
		return 0
	}
	for i := range bf.CpuBuffers {
		typeSize := int(bf.TypeSize[i])
		data := bf.CpuBuffers[i][0 : typeSize*bf.NumSprites]
		updateGLBuffer(bf.GpuBuffers[i], data, 0)
		dataMoved += typeSize * bf.NumSprites
	}

	return dataMoved
}

// BufferList needs a destructor because gpu arrays must be manually deleted
func (bf *BufferList) Delete() {
	gl.DeleteBuffers(int32(len(bf.GpuBuffers)), &bf.GpuBuffers[0])
}

// Allocate a buffer object
func newGLBuffer(size, typesize int, location uint32) uint32 {
	var vbo uint32
	gl.GenBuffers(1, &vbo)
	gl.BindBuffer(gl.ARRAY_BUFFER, vbo)
	gl.VertexAttribPointer(location, int32(typesize), gl.FLOAT, false, 0, nil)
	gl.EnableVertexAttribArray(location)
	gl.BufferData(gl.ARRAY_BUFFER, 4*size, gl.Ptr(nil), gl.DYNAMIC_DRAW) // 4 bytes in a float32
	// this sets the buffer index to move once per instance instead of once per vertex so all
	// vertices in the instance get the same value
	gl.VertexAttribDivisor(location, 1)
	return vbo
}

// Create template buffer that holds 6 vertices that form a two oposite triangles which make a
// sprite. By convention, shaders are expected to get their vertices at location 0.
func createSpriteVertexBuffer() uint32 {
	var vbo uint32
	gl.GenBuffers(1, &vbo)
	gl.BindBuffer(gl.ARRAY_BUFFER, vbo)
	gl.VertexAttribPointer(0, 3, gl.FLOAT, false, 0, nil)
	gl.EnableVertexAttribArray(0)
	gl.BufferData(gl.ARRAY_BUFFER, 4*len(spriteTemplateCentered), gl.Ptr(&spriteTemplateCentered[0]), gl.STATIC_DRAW)
	return vbo
}

// Move data to GL buffer
func updateGLBuffer(glbuffer uint32, newData []float32, position int) {
	gl.BindBuffer(gl.ARRAY_BUFFER, glbuffer)
	gl.BufferSubData(gl.ARRAY_BUFFER, 4*position, 4*len(newData), gl.Ptr(newData))
}
