package sprite

// SpriteDrawer ensures that a type has the Draw method.
type SpriteDrawer interface {
	Draw() // if the type has sprites, call their QueueRender
}
