package sprite

import (
	"fmt"

	"github.com/go-gl/gl/v4.6-core/gl"
	"gitlab.com/onikolas/agl/platform"
	"gitlab.com/onikolas/math"
)

var OpenGLInitialized bool

// A renderer holds a collection of BufferLists and renders them in a specific order
type Renderer struct {
	buffers          []*BufferList
	bufferSize       int
	bgColor          [4]float32
	viewMatrix       [16]float32
	projectionMatrix [16]float32
	projectionCenter math.Vector2[float32]
	zNear, zFar      float32
	windowSize       math.Vector2[int]

	// Stats
	SpritesRendered int
	DataMoved       int //float32s
}

// Create a new Renderer with the following default settings: alpha blending enabled, depth testing
// enabled, window size set to match the containing window size.
func NewRenderer() *Renderer {
	r := &Renderer{}
	r.Init()
	w, h := platform.GetWindowSize()
	r.Resize(int(w), int(h))
	return r
}

// Initialize rendering including opengl. Needs active opengl context (e.g. sdl.GLCreateContext)
// TODO: Deal with a opengl being already initialized (if the package is importerd in another rendering system)
func (r *Renderer) Init() {
	if err := gl.Init(); err != nil {
		panic(err)
	}

	r.bgColor = [4]float32{0.6, 0.1, 0.6, 1}

	r.viewMatrix = [16]float32{
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1,
	}

	r.bufferSize = DefaultBufferSize
	r.EnableAlphaBlending()
	r.EnableDepthTesting()
	r.SetBGColor(r.bgColor)
	r.SetProjection(math.Vector2[float32]{0, 0}, -10000, 10000)
	r.Resize(256, 256)
	OpenGLInitialized = true
}

// QueueRender adds a sprite to be rendered.
func (r *Renderer) QueueRender(sprite *Sprite) {
	// sprite's first render, cache its target buffer
	if sprite.bufferIndex < 0 {
		for i, v := range r.buffers {
			if v.RenderOrder == sprite.renderOrder &&
				v.Shader.Id == sprite.shader.Id &&
				sprite.atlas.atlasTexture == v.Texture {
				sprite.bufferIndex = i
			}
		}
		// no match found, allocate one
		if sprite.bufferIndex < 0 {
			buf, _ := NewBufferList(*sprite.shader, r.bufferSize, sprite.renderOrder, sprite.atlas.atlasTexture)
			sprite.bufferIndex = r.addBufferList(buf)
		}
	}
	r.buffers[sprite.bufferIndex].AddSprite(sprite)
}

// Add buffer store to the renderer. Buffers are inserted by ascending render order. Returns the
// inserted position in the buffers list.
func (r *Renderer) addBufferList(store *BufferList) int {
	// find the correct index to insert
	var index int
	for ; index < len(r.buffers); index++ {
		if r.buffers[index].RenderOrder > store.RenderOrder {
			break
		}
	}

	if index == len(r.buffers) {
		r.buffers = append(r.buffers, store)
	} else {
		r.buffers = append(r.buffers[:index+1], r.buffers[index:]...)
		r.buffers[index] = store
	}
	return index
}

// Set the size for newly created buffers. If not set SetBufferSize, buffers are created with with
// DefaultBufferSize capacity. This sets the upper limit for sprites that can be drawn within each
// buffer. The total sprite upper limit depends on the number of shaders and render orders
// used. Each unique combination of shader-renderOrder gets its own buffer.
func (r *Renderer) SetBufferSize(s int) {
	r.bufferSize = s
}

func (r *Renderer) SetBGColor(rgba [4]float32) {
	r.bgColor = rgba
	gl.ClearColor(r.bgColor[0], r.bgColor[1], r.bgColor[2], r.bgColor[3])
}

// Renders all buffers in the order specified by ShaderBuffer.RenderOrder.
func (r *Renderer) Render() {
	// background
	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

	r.SpritesRendered = 0
	r.DataMoved = 0

	// render every sprite store in RenderOrder
	for _, bf := range r.buffers {
		// move data from cpu to gpu
		r.DataMoved += bf.MoveCpuToGpu()

		gl.UseProgram(bf.Shader.Program)
		gl.BindVertexArray(bf.VAO)
		gl.BindTexture(gl.TEXTURE_2D, bf.Texture)

		if err := bf.Shader.UpdateUniform("view", r.viewMatrix[:]); err != nil {
			panic(err)
		}
		if err := bf.Shader.UpdateUniform("projection", r.projectionMatrix[:]); err != nil {
			panic(err)
		}

		gl.DrawArraysInstanced(gl.TRIANGLES, 0, 6, int32(bf.NumSprites))
		r.SpritesRendered += bf.NumSprites
		bf.Empty()
		gl.BindVertexArray(0)
	}
	platform.GLSwap()
}

// Resize the render area. Called after the render window has been resized
func (r *Renderer) Resize(w, h int) {
	r.windowSize.X, r.windowSize.Y = w, h
	r.setProjectionMatrix()
	gl.Viewport(0, 0, int32(w), int32(h))
}

// Set the camera to position (translate the whole scene)
func (r *Renderer) SetCamera(position math.Vector3[float32]) {
	r.viewMatrix[12] = -position.X
	r.viewMatrix[13] = -position.Y
	r.viewMatrix[14] = -position.Z
}

// Get the camera position
func (r *Renderer) GetCamera() math.Vector3[float32] {
	return math.Vector3[float32]{
		X: r.viewMatrix[12],
		Y: r.viewMatrix[13],
		Z: r.viewMatrix[14],
	}
}

// Disable depth testing. You can still control front to back order for whole buffers with
// BufferList.RenderOrder.
func (r *Renderer) DisableDepthTesting() {
	gl.Disable(gl.DEPTH_TEST)
}

// Enable depth testing. A sprite's z value determines it's depth.
func (r *Renderer) EnableDepthTesting() {
	gl.Enable(gl.DEPTH_TEST)
}

func (r *Renderer) DisableAlphaBlending() {
	gl.Disable(gl.BLEND)
}

func (r *Renderer) EnableAlphaBlending() {
	gl.BlendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA)
	gl.Enable(gl.BLEND)
}

// Sets an orthographic projection and places the origin at the point specified by center. Center
// must be a point in the range (0,0) to (1,1) where (0,0) is the bottom-left of the view window and
// (1,1) is the upper right. Setting center to (0,0) will make it so that origin is at the
// lower-left of the screen. Setting it to (0.5, 0.5) will make it so that origin is at the center
// of the window. Parameters zNear, zFar define the depth range that sprites can take.
func (r *Renderer) SetProjection(center math.Vector2[float32], zNear, zFar float32) {
	r.projectionCenter = center
	r.zFar = zFar
	r.zNear = zNear
	r.setProjectionMatrix()
}

func (r *Renderer) setProjectionMatrix() {
	fw, fh := float32(r.windowSize.X), float32(r.windowSize.Y)
	r.projectionMatrix = Ortho(
		-fw*r.projectionCenter.X, fw*(1-r.projectionCenter.X),
		-fh*r.projectionCenter.Y, fh*(1-r.projectionCenter.Y),
		r.zNear, r.zFar)
}

// Returns the screen bounding box. P1 is the lower-left and P2 is the upper-right.
func (r *Renderer) GetScreenBoundingBox() math.Box2D[float32] {
	fw, fh := float32(r.windowSize.X), float32(r.windowSize.Y)
	return math.Box2D[float32]{}.New(-fw*r.projectionCenter.X, -fh*r.projectionCenter.Y,
		fw*(1-r.projectionCenter.X), fh*(1-r.projectionCenter.Y))
}

func (r *Renderer) EnableBackFaceCulling() {
	gl.Enable(gl.CULL_FACE)
	gl.CullFace(gl.BACK)
}

func (r *Renderer) DisableBackFaceCulling() {
	gl.Disable(gl.CULL_FACE)
}

// Returns some data about the buffers renderer is using. For each buffer we report the number of
// sprites stored, the shader used and the render order. The number of sprites can only be read
// before Render is called because buffers are cleared after rendering.
func (r *Renderer) BufferStats() string {
	capacity := 0
	if len(r.buffers) > 0 {
		capacity = r.buffers[0].MaxSprites
	}
	str := fmt.Sprintf("#Buffers:%d Capacity:%d ", len(r.buffers), capacity)
	for i := range r.buffers {
		str += fmt.Sprintf("!buf%d/%d/%d/%d ", i,
			r.buffers[i].NumSprites,
			r.buffers[i].Shader.Id,
			r.buffers[i].RenderOrder)
	}
	return str
}
