package sprite

import (
	"fmt"

	"github.com/go-gl/gl/v4.6-core/gl"
)

func GetGLError() error {
	err := gl.GetError()
	if err != 0 {
		return fmt.Errorf("GL error %v", err)
	}
	return nil
}
