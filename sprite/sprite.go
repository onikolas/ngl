package sprite

import (
	"fmt"

	"gitlab.com/onikolas/agl/color"
	"gitlab.com/onikolas/agl/shaders"
	"gitlab.com/onikolas/ds"
	"gitlab.com/onikolas/math"
)

const NoopSprite = -1 //sprites with this Id dont get drawn

// Indexing of common shader data. These MUST match the order of shaders.CommonAttributes
const (
	uvIndex = iota
	colorIndex
	translationIndex
	rotateIndex
	scaleIndex
)

// Sprite holds info used to render a sprite. It exposes functions to manipulate sprite attributes
// such as position and scale and internally converts these into arrays suitable for rendering in
// OpenGL. Sprites are drawn from a sprite atlas stored in a sprite store and don't store pixel data
// on their own. To render a sprite it must be added to a Renderer using QueueRender.
type Sprite struct {
	index       int             // index in the atlas
	renderOrder int             // must match the renderOrder in one of the BufferLists
	atlas       *Atlas          // originating SpriteAtlas
	shader      *shaders.Shader // shader used to shade this Sprite
	shaderData  []*shaders.ShaderAttribute
	bufferIndex int
}

func NewSprite(spriteId int, spriteAtlas *Atlas, shader *shaders.Shader, renderOrder int) (Sprite, error) {
	spr := Sprite{
		index:       spriteId,
		shaderData:  []*shaders.ShaderAttribute{},
		atlas:       spriteAtlas,
		shader:      shader,
		bufferIndex: -1, // negative means the renderer  hasn't assigned a BufferList for us yet
		renderOrder: renderOrder,
	}

	// sort shader attributes by their location, this way sprite attributes order implicitly matches
	// BufferList arrays which are sorted in the same way
	attrKeys := ds.SortMapByValue(shader.Attributes, func(a, b shaders.ShaderAttribute) bool {
		return a.Location < b.Location
	})

	// allocate arrays for each attribute
	for _, v := range attrKeys {
		attr := shader.Attributes[v]
		if attr.Name == "vertex" {
			continue
		}
		newAttr := attr.Copy()
		spr.shaderData = append(spr.shaderData, &newAttr)
	}

	spr.SetIndex(spriteId)

	// set original size as the default
	spr.SetOriginalSize()

	return spr, nil
}

func (s Sprite) String() string {
	originalSize := s.GetOriginalSize()
	s1 := fmt.Sprintf("Index:%v\nOrig.Size:%v\nRenderOrder:%v\n", s.index, originalSize, s.renderOrder)

	s2 := ""
	for _, v := range s.shaderData {
		s2 += fmt.Sprintf("%v:%v\n", v.Name, v.Data)
	}

	return s1 + s2
}

// *** Setters needed because updating sprite parameters must also update shaderdata ***

// Setter for sprite position. Position is given in OpenGL coordinates, with the origin being in the
// center of the screen.
func (s *Sprite) SetPosition(position math.Vector3[float32]) {
	s.shaderData[translationIndex].Data[0] = position.X
	s.shaderData[translationIndex].Data[1] = position.Y
	s.shaderData[translationIndex].Data[2] = position.Z
}

func (s *Sprite) GetPosition() math.Vector3[float32] {
	return math.Vector3[float32]{
		X: s.shaderData[translationIndex].Data[0],
		Y: s.shaderData[translationIndex].Data[1],
		Z: s.shaderData[translationIndex].Data[2],
	}
}

// Setter for sprite rotation
func (s *Sprite) SetRotation(rot math.Vector3[float32]) {
	s.shaderData[rotateIndex].Data[0] = rot.X
	s.shaderData[rotateIndex].Data[1] = rot.Y
	s.shaderData[rotateIndex].Data[2] = rot.Z
}

func (s *Sprite) GetRotation() math.Vector3[float32] {
	return math.Vector3[float32]{
		X: s.shaderData[rotateIndex].Data[0],
		Y: s.shaderData[rotateIndex].Data[1],
		Z: s.shaderData[rotateIndex].Data[2],
	}
}

// Setter for sprite scale.
func (s *Sprite) SetScale(scale math.Vector2[float32]) {
	s.shaderData[scaleIndex].Data[0] = scale.X
	s.shaderData[scaleIndex].Data[1] = scale.Y
}

func (s *Sprite) GetScale() math.Vector2[float32] {
	return math.Vector2[float32]{
		X: s.shaderData[scaleIndex].Data[0],
		Y: s.shaderData[scaleIndex].Data[1],
	}
}

// Sets the sprite size to its original value as it appears on the atlas map
func (s *Sprite) SetOriginalSize() {
	orig := math.Vector2ConvertType[int, float32](s.GetOriginalSize())
	s.SetScale(orig)
}

// Get the sprite's original size which is its bounding box on the atlas map
func (s *Sprite) GetOriginalSize() math.Vector2[int] {
	bb := s.atlas.GetBoundingBox(s.index)
	return bb.Size()
}

// Set the sprite color. The same color is applied to every vertice.
func (s *Sprite) SetColor(color color.ColorRGBA) {
	s.shaderData[colorIndex].Data[0] = color.R
	s.shaderData[colorIndex].Data[1] = color.G
	s.shaderData[colorIndex].Data[2] = color.B
	s.shaderData[colorIndex].Data[3] = color.A
}

func (s *Sprite) GetColor() color.ColorRGBA {
	return color.NewColorRGBAFromSlice(s.shaderData[colorIndex].Data[0:4])
}

func (s *Sprite) GetIndex() int {
	return s.index
}

func (s *Sprite) SetIndex(index int) {
	s.index = index
	uvs := s.atlas.GetSpriteUVs(index)
	copy(s.shaderData[uvIndex].Data, uvs[:])
}

// General setter for sprite attributes.
func (s *Sprite) SetAttribute(location int, data []float32) {
	if location >= len(s.shaderData) || location < 0 {
		return
	}
	for i := 0; i < math.Min[int](len(s.shaderData[location].Data), len(data)); i++ {
		s.shaderData[location].Data[i] = data[i]
	}
}

// General getter for sprite attributes. Does not copy so use with care.
func (s *Sprite) GetAttribute(location int) []float32 {
	if location >= len(s.shaderData) || location < 0 {
		return nil
	}
	return s.shaderData[location].Data
}
