// Platform abstracts platoform specific actions such as creating a window and gathering input
// events. It is currently implemented with SDL2 but the intent is to encapsulate all SDL
// functionality and and make it easy to swap out for other platform tools if needed. The platform
// package does not define its own abstactions. Instead it conforms to the needs of other ngl
// packages. For example, although it needs a keypress abstraction, it uses that of the input
// package.
package platform
