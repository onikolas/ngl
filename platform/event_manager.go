package platform

import (
	"time"

	"github.com/veandco/go-sdl2/sdl"
	input "gitlab.com/onikolas/agl/input_parser"
	"gitlab.com/onikolas/math"
)

type EventManager struct {
	lastModState sdl.Keymod

	// stored events
	keypress     []input.KeyboardEvent
	resizeEvents []math.Vector2[int]
	quit         bool
}

// GatherEvents parses sdl events and stores them internally. The events can be accessed using
// EventManager's Get functions.  Currently handles keyboard input, window resizing and the quit
// event.
//
// # Keyboard events are parsed and converted into a list of keypresses as specified in the input package
//
// Window resize events are simply stored as a list of points.
func (m *EventManager) GatherEvents() {
	for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
		switch t := event.(type) {
		case *sdl.QuitEvent:
			m.handleQuitEvent()
		case *sdl.KeyboardEvent:
			m.handleKeypress(t)
		case *sdl.TextInputEvent:
			m.handleTextInput(t)
		case *sdl.WindowEvent:
			m.handleWindowEvent(t)
		}
	}
}

func (m *EventManager) GetKeyboardEvents() []input.KeyboardEvent {
	t := make([]input.KeyboardEvent, len(m.keypress))
	copy(t, m.keypress)
	m.keypress = m.keypress[:0]
	return t
}

func (m *EventManager) GetResizeEvents() []math.Vector2[int] {
	t := make([]math.Vector2[int], len(m.resizeEvents))
	copy(t, m.resizeEvents)
	m.resizeEvents = m.resizeEvents[:0]
	return t
}

func (m *EventManager) GetQuitEvent() bool {
	return m.quit
}

func (m *EventManager) handleKeypress(t *sdl.KeyboardEvent) {
	modstate := sdl.GetModState()

	if isSet(modstate, sdl.KMOD_CTRL) && !isSet(m.lastModState, sdl.KMOD_CTRL) {
		m.keypress = append(m.keypress, input.KeyboardEvent{Key: input.KeyControlDown})
	}
	if !isSet(modstate, sdl.KMOD_CTRL) && isSet(m.lastModState, sdl.KMOD_CTRL) {
		m.keypress = append(m.keypress, input.KeyboardEvent{Key: input.KeyControlUp})
	}

	if isSet(modstate, sdl.KMOD_ALT) && !isSet(m.lastModState, sdl.KMOD_ALT) {
		m.keypress = append(m.keypress, input.KeyboardEvent{Key: input.KeyAltDown})
	}
	if !isSet(modstate, sdl.KMOD_ALT) && isSet(m.lastModState, sdl.KMOD_ALT) {
		m.keypress = append(m.keypress, input.KeyboardEvent{Key: input.KeyAltUp})
	}

	// Characters in compound m.keypress (e.h ^C+c, ^C+v). Only activates when mod keys are active since normal
	// character input is handled by sdl.TextInputEvent. Ignoring the shift key.
	if modstate != 0 && t.Keysym.Sym >= sdl.K_a && t.Keysym.Sym <= sdl.K_z && t.Type == sdl.KEYDOWN &&
		!isSet(m.lastModState, sdl.KMOD_SHIFT) {
		m.keypress = append(m.keypress, input.KeyboardEvent{Key: input.KeyText, Text: string(rune(t.Keysym.Sym))})
	}

	if t.Keysym.Sym == sdl.K_RETURN && t.Type == sdl.KEYDOWN {
		m.keypress = append(m.keypress, input.KeyboardEvent{Key: input.KeyNewline, Text: "\n"})
	}
	if t.Keysym.Sym == sdl.K_BACKSPACE && t.Type == sdl.KEYDOWN {
		m.keypress = append(m.keypress, input.KeyboardEvent{Key: input.KeyBackspace})
	}
	if t.Keysym.Sym == sdl.K_DELETE && t.Type == sdl.KEYDOWN {
		m.keypress = append(m.keypress, input.KeyboardEvent{Key: input.KeyDelete})
	}

	// direction keys
	if t.Keysym.Sym == sdl.K_LEFT && t.Type == sdl.KEYDOWN {
		m.keypress = append(m.keypress, input.KeyboardEvent{Key: input.KeyLeft})
	}
	if t.Keysym.Sym == sdl.K_RIGHT && t.Type == sdl.KEYDOWN {
		m.keypress = append(m.keypress, input.KeyboardEvent{Key: input.KeyRight})
	}
	if t.Keysym.Sym == sdl.K_UP && t.Type == sdl.KEYDOWN {
		m.keypress = append(m.keypress, input.KeyboardEvent{Key: input.KeyUp})
	}
	if t.Keysym.Sym == sdl.K_DOWN && t.Type == sdl.KEYDOWN {
		m.keypress = append(m.keypress, input.KeyboardEvent{Key: input.KeyDown})
	}

	m.lastModState = modstate

	for _, v := range m.keypress {
		v.Timestamp = time.Now()
	}
}

func (m *EventManager) handleTextInput(t *sdl.TextInputEvent) {
	m.keypress = append(m.keypress, input.KeyboardEvent{Key: input.KeyText, Text: t.GetText(), Timestamp: time.Now()})

}

func (m *EventManager) handleWindowEvent(t *sdl.WindowEvent) {
	if t.Event == sdl.WINDOWEVENT_RESIZED {
		m.resizeEvents = append(m.resizeEvents, math.Vector2[int]{X: int(t.Data1), Y: int(t.Data2)})
	}
}

func (m *EventManager) handleQuitEvent() {
	m.quit = true
}

// Is this modifier (ctrl, alt, etc) set ?
func isSet(state, mod sdl.Keymod) bool {
	return (state & mod) != 0
}
