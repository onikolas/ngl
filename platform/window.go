package platform

import (
	"os"
	"runtime"

	"github.com/veandco/go-sdl2/sdl"
)

var window *sdl.Window

// InitializeWindow a window of width x height and set its name
func InitializeWindow(width, height int, name string, resizeable bool, fullscreen bool) error {
	// already initialized
	if window != nil {
		return nil
	}

	// sdl might crash otherwise
	runtime.LockOSThread()

	if err := sdl.Init(sdl.INIT_EVERYTHING); err != nil {
		return err
	}

	var options uint32 = sdl.WINDOW_OPENGL

	if resizeable {
		options = options | sdl.WINDOW_RESIZABLE
	}

	if fullscreen {
		options = options | sdl.WINDOW_FULLSCREEN
	}

	win, err := sdl.CreateWindow(name, sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED,
		int32(width), int32(height), options)
	if err != nil {
		return err
	}

	if _, err = win.GLCreateContext(); err != nil {
		return err
	}

	// Update whenever ready. Passing 1 will vsync
	sdl.GLSetSwapInterval(0)

	window = win
	return nil
}

// Have we already intitialized?
func IsInitialized() bool {
	return window != nil
}

// GetWindowSize returns the window's dimensions
func GetWindowSize() (width, height int32) {
	if window == nil {
		return -1, -1
	}
	return window.GetSize()
}

func GLSwap() {
	window.GLSwap()
}

func Quit() {
	window.Destroy()
	sdl.Quit()
	os.Exit(0)
}
