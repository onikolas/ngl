package platform

import (
	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/onikolas/math"
)

// InputState holds state information about input devices. Used as a low-level alternative to
// EventManager when using events is not sufficient like game input or typing with a custom repeat
// frequency.
type InputState struct {
	keyboardState []uint8
	mouseState    uint32
	mouseX        int32
	mouseY        int32
}

func NewInputState() *InputState {
	return &InputState{
		keyboardState: sdl.GetKeyboardState(),
	}
}

// Refresh input state. Call this in a loop.
func (s *InputState) GetState() {
	sdl.PumpEvents()
	s.mouseX, s.mouseY, s.mouseState = sdl.GetMouseState()
}

// Returns 1 if the key is pressed 0 otherwise.
func (s *InputState) KeyValue(key KeyboardKey) float32 {
	return float32(s.keyboardState[key])
}

// Returns true if the key is pressed false otherwise.
func (s *InputState) KeyDown(key KeyboardKey) bool {
	return s.keyboardState[key] > 0
}

// Returns the mouse position normalized by the window dimentions.
func (s *InputState) MousePosition() math.Vector2[float32] {
	_, h := window.GetSize()
	return math.Vector2[float32]{
		X: float32(s.mouseX),
		Y: float32(h - s.mouseY),
	}
}

// Check if the mouse button is pressed.
func (s *InputState) MouseButtonDown(button MouseButton) bool {
	return (1<<button)&s.mouseState != 0
}

// Returns 1 if the button is pressed 0 otherwise.
func (s *InputState) MouseButtonValue(button MouseButton) float32 {
	return float32((1 << button) & s.mouseState)
}

type KeyboardKey int

const (
	KeyboardA = iota + sdl.SCANCODE_A
	KeyboardB
	KeyboardC
	KeyboardD
	KeyboardE
	KeyboardF
	KeyboardG
	KeyboardH
	KeyboardI
	KeyboardJ
	KeyboardK
	KeyboardL
	KeyboardM
	KeyboardN
	KeyboardO
	KeyboardP
	KeyboardQ
	KeyboardR
	KeyboardS
	KeyboardT
	KeyboardU
	KeyboardV
	KeyboardW
	KeyboardX
	KeyboardY
	KeyboardZ
)

const (
	Keyboard1 = iota + sdl.SCANCODE_1
	Keyboard2
	Keyboard3
	Keyboard4
	Keyboard5
	Keyboard6
	Keyboard7
	Keyboard8
	Keyboard9
	Keyboard0
)

const (
	KeyboardReturn = iota + sdl.SCANCODE_RETURN
	KeyboardEscape
	KeyboardBackspace
	KeyboardTab
	KeyboardSpace
)

const (
	KeyboardRight = iota + sdl.SCANCODE_RIGHT
	KeyboardLeft
	KeyboardDown
	KeyboardUp
)

const (
	KeyboardLCtrl = iota + sdl.SCANCODE_LCTRL
	KeyboardLShift
	KeyboardLAlt
	KeyboardLMeta
	KeyboardRCtrl
	KeyboardRShift
	KeyboardRAlt
)

type MouseButton int

const (
	MouseLeft MouseButton = iota
	MouseMiddle
	MouseRight
	MouseButtonDown
	MouseButtonUp
)
