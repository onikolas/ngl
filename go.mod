module gitlab.com/onikolas/agl

go 1.22.0

toolchain go1.22.5

require (
	github.com/go-gl/gl v0.0.0-20231021071112-07e5d0ea2e71
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0
	github.com/veandco/go-sdl2 v0.4.40
	gitlab.com/onikolas/gapbuffer v0.0.0-20221003215411-1a35e714499d
	gitlab.com/onikolas/math v0.0.0-20240915173508-299a98af8e15
	golang.org/x/exp v0.0.0-20240909161429-701f63a606c0
	golang.org/x/image v0.20.0
)

require gitlab.com/onikolas/ds v0.0.0-20230808130118-53c60ec78bef
