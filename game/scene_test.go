package game

import (
	"fmt"
	"testing"
	"time"
)

var visited []string

type GameObjectTest struct {
	a string
	GameObjectCommon
}

func (g *GameObjectTest) Init() {
	visited = append(visited, g.a)
}

func (g *GameObjectTest) Update(depth int, dt time.Duration) {
	depths = append(depths, depth)
}

func (g *GameObjectTest) Render() {}

func TestSceneInit(t *testing.T) {
	scene := Scene{}

	go1 := GameObjectTest{a: "go1"}
	go2 := GameObjectTest{a: "go2"}
	go3 := GameObjectTest{a: "go3"}
	go2a := GameObjectTest{a: "go2a"}
	go2b := GameObjectTest{a: "go2b"}

	scene.AddGameObject(&go1)
	scene.AddGameObject(&go2)
	scene.AddGameObject(&go3)
	go2.AddChild(&go2a)
	go2.AddChild(&go2b)

	scene.Init()
	expect := []string{"go1", "go2", "go2a", "go2b", "go3"}
	for i := range visited {
		if visited[i] != expect[i] {
			t.Error(visited, expect)
		}
	}
}

var depths []int

func TestSceneUpdate(t *testing.T) {
	scene := Scene{}
	go1 := GameObjectTest{a: "go1"}
	go2 := GameObjectTest{a: "go2"}
	go2a := GameObjectTest{a: "go2a"}
	go2b := GameObjectTest{a: "go2b"}
	go2aa := GameObjectTest{a: "go2aa"}

	scene.AddGameObject(&go1)
	scene.AddGameObject(&go2)
	go2.AddChild(&go2a)
	go2.AddChild(&go2b)
	go2a.AddChild(&go2aa)

	scene.Update(0)
	expect := []int{0, 0, -1, -2, -1}
	for i := range depths {
		if depths[i] != expect[i] {
			t.Error(fmt.Sprintln(depths, expect))
			t.FailNow()
		}
	}
}
