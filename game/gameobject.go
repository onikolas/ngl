package game

import (
	"time"

	"gitlab.com/onikolas/agl/sprite"
)

// GameObject is any entity in the game. GameObject are arranged in a tree hierachy meaning an
// object has one parent and many children.
type GameObject interface {
	// Initialize this gameobject.
	Init()
	// Has this GameObject been initialized?
	Initialized() bool

	// Update gameobject logic. dt is the elapsed time since the last update.
	Update(dt time.Duration)
	// Render the gameobject.
	Render(*sprite.Renderer)
	// A game object is destroyed by unlinking it from its parent so it doesn't receive
	// updates. Resources will be freed with garbage collection if there are no other references
	Destroy()
	// Register a function to run when the game object is destroyed
	RunOnDestroy(func())

	// return GameObject's id.
	Id() int

	// Get child Gameobjects.
	GetChildren() []GameObject
	// Add a GameObject as child. Implementation should also SetParent() itself.
	AddChild(g GameObject)
	// Remove child with this id
	RemoveChild(id int)

	// Get parent of this GameObject
	GetParent() GameObject
	// Set this GameObject's parent.
	SetParent(GameObject)

	// Get a ref to this gameobject.
	GameObject() GameObject

	// Get Tags on this GameObject
	GetTags() []int
	// Add a tag to this GameObject
	AddTag(int)

	Transformer
}

// Spawn game object g into scene. Using Spawn ensures the object has been initialized.
func Spawn(g GameObject, scene *Scene) {
	g.Init()
	scene.AddGameObject(g)
}

// Spawn game object g as a child of parent. Using SpawnChild ensures the object has been initialized.
func SpawnChild(g, parent GameObject) {
	g.Init()
	parent.AddChild(g)
}
