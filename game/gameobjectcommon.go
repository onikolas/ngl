package game

import (
	"time"

	"gitlab.com/onikolas/agl/sprite"
	"gitlab.com/onikolas/math"
)

var commonIdSource int = 0 //0 is uninitialized ids
func nextId() int {
	commonIdSource += 1
	return commonIdSource
}

// GameObjectCommon implements the GameObject interface and can be included in user-created game
// objects as a starting point.
type GameObjectCommon struct {
	id          int
	initialized bool

	translation math.Vector3[float32]
	rotation    math.Vector3[float32]
	scale       math.Vector2[float32]

	children       []GameObject
	parent         GameObject
	tags           []int
	onDestroyFuncs []func()

	embededIn GameObject
}

// Create a new GameObjectCommon. If GameObjectCommon is embedded in another struct you can pass that in and
// GameObjectCommon will use that for creating associations (e.g parent/child) to other GameObjects. You
// can also pass in nil.
func NewGameObjectCommon(embededIn GameObject) GameObjectCommon {
	goc := GameObjectCommon{embededIn: embededIn}
	goc.Init()
	return goc
}

// Will return the object that embeds GameObjectCommon if that was set otherwise it will return itself
func (g *GameObjectCommon) GameObject() GameObject {
	if g.embededIn != nil {
		return g.embededIn
	}
	return g
}

func (g *GameObjectCommon) Init() {
	if g.initialized {
		return
	}
	g.id = nextId()
	g.initialized = true
}

func (g *GameObjectCommon) Initialized() bool {
	return g.initialized
}

func (g *GameObjectCommon) Render(r *sprite.Renderer) {}
func (g *GameObjectCommon) Update(dt time.Duration)   {}

func (g *GameObjectCommon) AddChild(gg GameObject) {
	if !gg.Initialized() {
		gg.Init()
	}
	gg.SetParent(g.GameObject())
	g.children = append(g.children, gg)
}

func (g *GameObjectCommon) RemoveChild(id int) {
	for i := range g.children {
		if id == g.children[i].Id() {
			g.children[i] = g.children[len(g.children)-1]
			g.children = g.children[:len(g.children)-1]
			break
		}
	}
}

func (g *GameObjectCommon) Id() int                                { return g.id }
func (g *GameObjectCommon) GetChildren() []GameObject              { return g.children }
func (g *GameObjectCommon) GetParent() GameObject                  { return g.parent }
func (g *GameObjectCommon) SetParent(parent GameObject)            { g.parent = parent }
func (g *GameObjectCommon) GetTranslation() math.Vector3[float32]  { return g.translation }
func (g *GameObjectCommon) GetRotation() math.Vector3[float32]     { return g.rotation }
func (g *GameObjectCommon) GetScale() math.Vector2[float32]        { return g.scale }
func (g *GameObjectCommon) SetTranslation(t math.Vector3[float32]) { g.translation = t }
func (g *GameObjectCommon) SetRotation(r math.Vector3[float32])    { g.rotation = r }
func (g *GameObjectCommon) SetScale(s math.Vector2[float32])       { g.scale = s }

func (g *GameObjectCommon) Destroy() {
	depthFirst(g, func(a GameObject) {
		g.parent.RemoveChild(g.id)
		for i := range g.onDestroyFuncs {
			g.onDestroyFuncs[i]()
		}
	}, 0)
}

func (g *GameObjectCommon) RunOnDestroy(fn func()) {
	g.onDestroyFuncs = append(g.onDestroyFuncs, fn)
}

// GameObjectCommon is meant to be embedded in other objects. SetEmbed stores a ref to that object
// and that object is then used in place of GameObjectCommon when GameObjectCommon needs to return a ref of itself
func (g *GameObjectCommon) SetEmbed(gg GameObject) {
	g.embededIn = gg
}

// Get the GameObject that houses this GameObjectCommon
func (g *GameObjectCommon) GetEmbed() GameObject {
	return g.embededIn
}

func (g *GameObjectCommon) AddTag(tag int) {
	g.tags = append(g.tags, tag)
}

func (g *GameObjectCommon) GetTags() []int {
	return g.tags
}

func (g *GameObjectCommon) String() string {
	return "GameObjectCommon"
}
