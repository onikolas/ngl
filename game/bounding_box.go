package game

import (
	"time"

	"gitlab.com/onikolas/agl/sprite"
	"gitlab.com/onikolas/math"
)

var RenderBoundingBoxes bool

// BoundingBox is an axis-aligned bounding box used for collisions. BoundingBoxes can be static (e.g
// for gameobjects that do not move) in which case they do not need to do collision checks. Active
// (not static) bounding boxes check for collisions against other active bounding boxes as well as
// static bounding boxes so the cost of doing collisions is O(active^2 + active*static). Reducing
// the number of active bounding boxes increases performance.
type BoundingBox struct {
	math.Box2D[float32]

	Enabled bool

	id                int
	static            bool
	parent            GameObject
	translationAdjust math.Vector2[float32]
	sizeAdjust        math.Vector2[float32]
	sprite            *sprite.Sprite
	collisionSystem   *CollisionManagerAABB
}

// Check if colliding with other gameobjects
func (b *BoundingBox) CheckForCollisions() []GameObject {
	if b.static || b.parent == nil {
		return nil
	}

	collisions := []GameObject{}
	collisionBBs := b.collisionSystem.CheckCollision(b)
	for _, v := range collisionBBs {
		collisions = append(collisions, v.parent)
	}

	return collisions
}

func (b *BoundingBox) Update(dt time.Duration, g GameObject) {
	b.parent = g
	position := g.GetTranslation().Add(b.translationAdjust.AddZ(1))
	positionXY := position.XY()
	parentSize := g.GetScale()
	b.P1 = positionXY.Sub(parentSize.Scale(0.5).Add(b.sizeAdjust))
	b.P2 = positionXY.Add(parentSize.Scale(0.5).Add(b.sizeAdjust))
	b.MakeCanonical()
	if RenderBoundingBoxes && b.sprite != nil {
		b.sprite.SetPosition(position)
		b.sprite.SetScale(b.Size())
	}
}

// Set the size adjustment of this bounding box. The final size will be parentSize+scaleAdjust
func (b *BoundingBox) SetSizeAdjust(sizeAdjust math.Vector2[float32]) {
	b.sizeAdjust = sizeAdjust
	b.Update(0, b.parent)
}

// Set the position adjustment of this bounding box. The final position will be parentPosition+translationAdjust
func (b *BoundingBox) SetTranslationAdjust(translationAdjust math.Vector2[float32]) {
	b.translationAdjust = translationAdjust
	b.Update(0, b.parent)
}

func (b *BoundingBox) SetSprite(s *sprite.Sprite) {
	b.sprite = s
	b.Update(0, b.parent)
}

func (b *BoundingBox) Render(r *sprite.Renderer) {
	if RenderBoundingBoxes && b.sprite != nil {
		r.QueueRender(b.sprite)
	}
}

func (b *BoundingBox) Destroy() {
	b.collisionSystem.Delete(b)
}

func (b *BoundingBox) GetId() int {
	return b.id
}
