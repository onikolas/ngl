package game

import (
	"time"

	"gitlab.com/onikolas/agl/shaders"
	"gitlab.com/onikolas/agl/sprite"
	"gitlab.com/onikolas/math"
)

// Animation is a component that renders an animation (sprite sequence).
type Animation struct {
	run          bool
	clips        [][]sprite.Sprite
	activeClip   int
	activeSprite int
	waitTime     time.Duration
	frameTime    time.Duration

	translationAdjust math.Vector3[float32]
	scaleAdjust       math.Vector2[float32]
	rotationAdjust    math.Vector3[float32]
}

// Create an animation from a list of sprites.
func NewAnimation() Animation {
	a := Animation{}
	a.SetAnimationSpeed(1)
	a.translationAdjust = math.Vector3[float32]{0, 0, 0}
	a.scaleAdjust = math.Vector2[float32]{1, 1}
	a.rotationAdjust = math.Vector3[float32]{0, 0, 0}
	return a
}

// Adds an animation clip. Returns the clip index that can be set with SetClip
func (a *Animation) AddClip(sprites []int, atlas *sprite.Atlas, shader *shaders.Shader, renderOrder int) int {
	clip := []sprite.Sprite{}
	for i := range sprites {
		sprite, _ := sprite.NewSprite(sprites[i], atlas, shader, renderOrder)
		clip = append(clip, sprite)
	}
	a.clips = append(a.clips, clip)
	return len(a.clips) - 1
}

func (a *Animation) Render(renderer *sprite.Renderer) {
	renderer.QueueRender(&a.clips[a.activeClip][a.activeSprite])
}

func (a *Animation) Update(dt time.Duration, parent GameObject) {
	a.waitTime += dt

	if a.waitTime >= a.frameTime && a.run {
		a.activeSprite = (a.activeSprite + 1) % len(a.clips[a.activeClip])
		a.waitTime = a.waitTime - a.frameTime
	} else {
		// handles switching from larger clips
		a.activeSprite = a.activeSprite % len(a.clips[a.activeClip])
	}

	a.clips[a.activeClip][a.activeSprite].SetPosition(parent.GetTranslation().Add(a.translationAdjust))
	a.clips[a.activeClip][a.activeSprite].SetScale(parent.GetScale().Mul(a.scaleAdjust))
	a.clips[a.activeClip][a.activeSprite].SetRotation(parent.GetRotation().Add(a.rotationAdjust))
}

// Set the clip to play
func (a *Animation) SetClip(clip int) {
	clip = math.Clamp(clip, 0, len(a.clips)-1)
	a.activeClip = clip
}

// Set the animation speed in sprites/second
func (a *Animation) SetAnimationSpeed(sps float64) {
	a.frameTime = time.Duration((float64(time.Second) / sps))
}

// Run the animation. On creation, the animation is not running.
func (a *Animation) Run() {
	a.run = true
}

// Stop the animation and return to the starting frame.
func (a *Animation) Stop() {
	a.activeSprite = 0
	a.run = false
}

// Stop the animation at it's current frame.
func (a *Animation) Freeze() {
	a.run = false
}

// Return n-th sprite in the animation of a clip
func (a *Animation) GetSprite(clip, n int) sprite.Sprite {
	return a.clips[clip][n]
}

// Adjust sprite traslation (postiont). The sprite position is parentGameObject.GetTranslation()+adjust.
// The default translation  adjust is {0,0}, i.e same as the parent.
func (a *Animation) AdjustTranslation(adjust math.Vector3[float32]) {
	a.translationAdjust = adjust
}

// Adjust sprite scale. The sprite scale is parentGameObject.GetScale() * adjust.
// The default scale adjust is {1,1}, i.e same as the parent.
func (a *Animation) AdjustScale(adjust math.Vector2[float32]) {
	a.scaleAdjust = adjust
}

// Adjust sprite rotation. The sprite rotation is parentGameObject.GetRotation()+adjust.
// The default rotation adjust is {0,0}, i.e same as the parent.
func (a *Animation) AdjustRotation(adjust math.Vector3[float32]) {
	a.rotationAdjust = adjust
}

func (a *Animation) SpriteModifier(fn func(sprite *sprite.Sprite)) {
	// TODO store this fn and run on the active sprite on each update if not nil
}

func (a *Animation) ClipDurations() []int {
	durations := []int{}
	for i := range a.clips {
		durations = append(durations, len(a.clips[i]))
	}
	return durations
}
