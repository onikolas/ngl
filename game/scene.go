package game

import (
	"time"

	"gitlab.com/onikolas/agl/sprite"
	"gitlab.com/onikolas/math"
)

// Scene is a collection of GameObjects. It can representa level, a menu, a loading screen
// etc. GameObjects can have children which turns the scene organization into a tree.
type Scene struct {
	root GameObjectCommon
	// Stats from last call to update and render
	ObjectsUpdated, ObjectsRendered int
}

func NewScene() *Scene {
	s := Scene{}
	s.root.SetScale(math.Vector2[float32]{1, 1})
	return &s
}

// Add a gameobject to the root of the scene
func (s *Scene) AddGameObject(g GameObject) {
	if !g.Initialized() {
		g.Init()
	}
	s.root.AddChild(g)
}

// Update all gameobjects
func (s *Scene) Update(dt time.Duration) {
	fn := func(g GameObject) {
		g.Update(dt)
	}
	s.ObjectsUpdated = depthFirst(&s.root, fn, 0) - 1 //-1 to not count the root as an object
}

// Call GameObject.Render for all gameobjects in the scene.
func (s *Scene) Render(r *sprite.Renderer) {
	fn := func(g GameObject) {
		g.Render(r)
	}
	s.ObjectsRendered = depthFirst(&s.root, fn, 0) - 1
}

// Call Destroy on everything in the scene. Used when unloading a scene.
func (s *Scene) Destroy() {
	for _, v := range s.root.GetChildren() {
		v.Destroy()
	}
}

// TODO remove recursion and use a stack
func depthFirst(g GameObject, fn func(g GameObject), depth int) int {
	fn(g)
	calls := 1
	for _, v := range g.GetChildren() {
		calls += depthFirst(v, fn, depth+1)
	}
	return calls
}

func (s *Scene) String() string {
	return "scene"
}
