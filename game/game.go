package game

import (
	"fmt"
	"time"

	"gitlab.com/onikolas/agl/platform"
	"gitlab.com/onikolas/agl/sprite"
)

type Game struct {
	renderer   *sprite.Renderer
	Events     *platform.EventManager
	InputState *platform.InputState
	Collisions CollisionManagerAABB

	scene *Scene
}

// Setup game.
func (g *Game) Init(renderer *sprite.Renderer) {
	g.renderer = renderer
	g.Events = &platform.EventManager{}
	g.InputState = platform.NewInputState()
}

// Main game loop. Call once after setting the initial scene.
func (g *Game) Loop() {
	timer := time.Now()
	for {
		dt := time.Since(timer)
		timer = time.Now()

		g.Events.GatherEvents()
		g.InputState.GetState()
		for _, resize := range g.Events.GetResizeEvents() {
			fmt.Println(resize)
			g.renderer.Resize(resize.X, resize.Y)
		}

		if g.scene != nil {
			g.scene.Update(dt)
			g.scene.Render(g.renderer)
		}
		g.renderer.Render()
	}
}

// Set the game scene to play
func (g *Game) SetScene(scene *Scene) {
	g.scene = scene
}
