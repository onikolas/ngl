package game

import (
	"time"

	"gitlab.com/onikolas/agl/shaders"
	"gitlab.com/onikolas/agl/sprite"
	"gitlab.com/onikolas/agl/text"
	"gitlab.com/onikolas/math"
)

// Text is a component that draws text in a bounding box.
type Text struct {
	bb              math.Box2D[int]
	sprites         []sprite.Sprite
	spritePositions []math.Vector2[float32]
	font            text.Font
	text            string
	runeSpriteMap   *text.RuneToSpriteMap
	atlas           *sprite.Atlas
	shader          *shaders.Shader
	renderOrder     int
}

func NewText(text string, font text.Font, boundingBox math.Box2D[int], runeSpriteMap *RuneToSpriteMap,
	atlas *sprite.Atlas, shader *shaders.Shader, renderOrder int) *Text {
	t := Text{
		bb:            boundingBox,
		runeSpriteMap: runeSpriteMap,
		atlas:         atlas,
		shader:        shader,
		renderOrder:   renderOrder,
		font:          font,
	}
	t.SetText(text)
	return &t
}

// Sets the text in this component.
func (t *Text) SetText(text string) {
	// clear existing
	t.sprites = []sprite.Sprite{}
	t.spritePositions = []math.Vector2[float32]{}

	yAdvance := t.font.FontMetrics().YAdvance
	dot := t.bb.P3().Sub(math.Vector2[int]{0, yAdvance})
	for i, r := range []rune(text) {
		spriteId := t.runeSpriteMap.Get(r)
		if spriteId < 0 {
			continue
		}
		metrics := t.font.RuneMetrics(r)
		spr, _ := sprite.NewSprite(spriteId, t.atlas, t.shader, t.renderOrder)
		spr.SetScale(math.Vector2ConvertType[int, float32](metrics.BoundingBox.Size()))
		t.sprites = append(t.sprites, spr)
		spritePos := math.Vector2ConvertType[int, float32](dot)
		fAdjust := math.Vector2ConvertType[int, float32](metrics.Adjust)
		t.spritePositions = append(t.spritePositions, spritePos.Add(fAdjust))
		dot = dot.Add(math.Vector2[int]{X: metrics.Advance, Y: 0})
		if i < len(text)-1 {
			nextRune := text[i+1]
			nextAdvance := t.font.RuneMetrics(rune(nextRune)).Advance
			if dot.X+nextAdvance >= t.bb.Size().X {
				dot.X = 0
				dot.Y -= yAdvance
			}
		}
	}
}

func (t *Text) Update(dt time.Duration, parent GameObject) {
	for i := range t.sprites {
		t.sprites[i].SetPosition(parent.GetTranslation().Add(t.spritePositions[i].AddZ(0)))
		t.sprites[i].SetScale(parent.GetScale().Mul(t.sprites[i].GetScale()))
		t.sprites[i].SetRotation(parent.GetRotation())
	}
}

func (t *Text) Render(renderer *sprite.Renderer) {
	for i := range t.sprites {
		renderer.QueueRender(&t.sprites[i])
	}
}
