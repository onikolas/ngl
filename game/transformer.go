package game

import (
	"gitlab.com/onikolas/math"
)

// Transformer has a translation, rotation and scale.
type Transformer interface {
	GetTranslation() math.Vector3[float32]
	GetRotation() math.Vector3[float32]
	GetScale() math.Vector2[float32]
	SetTranslation(math.Vector3[float32])
	SetRotation(math.Vector3[float32])
	SetScale(math.Vector2[float32])
}

// Return a matrix representation of a Transformer.
func TransformerToMatrix(t Transformer) math.Matrix4[float32] {
	return math.NewMatrix4Transform[float32](
		t.GetTranslation(),
		t.GetScale().AddZ(1),
		t.GetRotation().X,
		t.GetRotation().Y,
		t.GetRotation().Z)
}

func Transform(t Transformer, vector math.Vector3[float32]) math.Vector3[float32] {
	mat := TransformerToMatrix(t)
	return mat.MulVector(vector)
}
