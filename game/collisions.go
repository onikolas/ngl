package game

import (
	"errors"
)

// Provides collision detection using Axis Aligned Bounding Boxes.
type CollisionManagerAABB struct {
	bbs []*BoundingBox
}

// Creates a new bounding box. It will be a dynamic bounding box unless static is true. It
// additinally returns the NewBoundingBox destroy function as a reminder to call it when done.
func (cm *CollisionManagerAABB) NewBoundingBox(static bool, parent GameObject) (*BoundingBox, func()) {
	bb := &BoundingBox{
		id:              nextId(),
		Enabled:         true,
		static:          static,
		collisionSystem: cm,
		parent:          parent,
	}
	parentXY := parent.GetTranslation().XY()
	parentSize := parent.GetScale()
	bb.P1 = parentXY.Sub(parentSize.Scale(0.5))
	bb.P2 = parentXY.Add(parentSize.Scale(0.5))
	cm.bbs = append(cm.bbs, bb)
	return bb, bb.Destroy
}

// Check if bb collides with other bounding boxes. If bb is static it doesn't collide with anything
// as only dynamic bounding boxes can check for collisions.
func (cm *CollisionManagerAABB) CheckCollision(bb *BoundingBox) []*BoundingBox {
	if bb.static {
		return nil
	}

	collisions := []*BoundingBox{}

	for _, other := range cm.bbs {
		if bb.Overlaps(other.Box2D) && bb.id != other.id && other.Enabled {
			collisions = append(collisions, other)
		}
	}

	return collisions
}

// Remove a bounding box from the manager
func (cm *CollisionManagerAABB) Delete(bbox *BoundingBox) error {
	for i := range cm.bbs {
		if cm.bbs[i].id == bbox.id {
			cm.bbs[i] = cm.bbs[len(cm.bbs)-1]
			cm.bbs = cm.bbs[:len(cm.bbs)-1]
			return nil
		}
	}
	return errors.New("Bounding box not found")
}

// Number of active bounding boxes
func (cm *CollisionManagerAABB) NumBoundingBoxes() int {
	return len(cm.bbs)
}
