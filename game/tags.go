package game

// Does game object have tag?
func HasTag(g GameObject, tag int) bool {
	for _, t := range g.GetTags() {
		if t == tag {
			return true
		}
	}
	return false
}
